// ============================================================================
// Include files
// ============================================================================
#ifdef __INTEL_COMPILER
#pragma warning(disable:1572) // non-pointer conversion ... may lose significant bits
#pragma warning(push)
#endif
#include "GaudiKernel/SerializeSTL.h"
// ============================================================================
// HltBase
// ============================================================================
#include "HltBase/HltUtils.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/VxCreator.h"
#include "LoKi/Combiner.h"
// ============================================================================
// local
// ============================================================================
#include "LTTools.h"
// ============================================================================
#ifdef __INTEL_COMPILER
#pragma warning(pop)
#endif
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hlt1::VxCreator
 *  @date 2008-11-14
 *  @author Vanya  BELYAEV Ivan.BElyaev@nikhef.nl
 */
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TrCuts&  cuts    ,
  const LoKi::Types::TTrCuts& cuts2tr ,
  const LoKi::Types::RVCuts&  cuts4rv )
  : LoKi::AuxFunBase ( std::tie ( cuts , cuts2tr , cuts4rv ) )
  //
  , m_cut    ( cuts    )
  , m_cut2tr ( cuts2tr )
  , m_cut4rv ( cuts4rv )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TTrCuts& cuts2tr ,
  const LoKi::Types::RVCuts&  cuts4rv )
  : LoKi::AuxFunBase ( std::tie ( cuts2tr , cuts4rv ) )
    //
  , m_cut2tr ( cuts2tr )
  , m_cut4rv ( cuts4rv )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TrCuts&  cuts    ,
  const LoKi::Types::RVCuts&  cuts4rv )
  : LoKi::AuxFunBase ( std::tie ( cuts , cuts4rv ) )
  , m_cut    ( cuts    )
  , m_cut4rv ( cuts4rv )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TrCuts&  cuts    ,
  const LoKi::Types::TTrCuts& cuts2tr )
  : LoKi::AuxFunBase ( std::tie ( cuts , cuts2tr ) )
  //
  , m_cut    ( cuts    )
  , m_cut2tr ( cuts2tr )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TrCuts& cuts )
  : LoKi::AuxFunBase ( std::tie ( cuts ) )
  , m_cut    ( cuts    )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::TTrCuts& cuts2tr )
  : LoKi::AuxFunBase ( std::tie ( cuts2tr ) )
  , m_cut2tr ( cuts2tr )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator
( const LoKi::Types::RVCuts&  cuts4rv )
  : LoKi::AuxFunBase ( std::tie ( cuts4rv ) )
  , m_cut4rv ( cuts4rv )
{}
// ============================================================================
// constructor from the cuts
// ============================================================================
LoKi::Hlt1::VxCreator::VxCreator ()
  : LoKi::AuxFunBase { std::tie() }
{}
// ============================================================================
// nice printout
// ============================================================================
std::ostream& LoKi::Hlt1::VxCreator::fillStream ( std::ostream& s ) const
{
  std::vector<std::string> out ;
  if (m_cut) out.push_back( m_cut->printOut() );
  if (m_cut2tr) out.push_back( m_cut2tr->printOut() );
  if (m_cut4rv) out.push_back( m_cut4rv->printOut() );
  return GaudiUtils::details::ostream_joiner( s << "LoKi.Hlt1.VxCreator(" ,
                                              out, "," ) << ")";
}
// ============================================================================
// make the vertices
// ============================================================================
size_t LoKi::Hlt1::VxCreator::make
( const LHCb::Track::ConstVector&  tracks1  ,
  const LHCb::Track::ConstVector&  tracks2  ,
  std::vector<LHCb::RecVertex*>&   vertices ) const
{
  if ( tracks1.empty() || tracks2.empty() ) { return 0 ; }

  const bool same =
    ( &tracks1 == &tracks2 ) || ( tracks1 == tracks2 ) ;

  const size_t n1 = tracks1.size() ;
  const size_t n2 = tracks2.size() ;

  vertices.reserve ( same ?
                     vertices.size() + n1*(n1-1)/2 :
                     vertices.size() + n1*n2       ) ;

  typedef LHCb::Track::ConstVector   Tracks   ;
  typedef LoKi::Combiner_<Tracks>    Combiner ;

  const Hlt::VertexCreator           creator    ;
  const LoKi::Hlt1::Utils::CmpTrack  compare = LoKi::Hlt1::Utils::CmpTrack()  ;

  const size_t size = vertices.size() ;

  /// create the combiner & fill it with data
  Combiner loop;
  loop.add ( { tracks1 }  ) ;
  loop.add ( { tracks2 }  ) ;

  // make the combinations
  for ( ; loop.valid() ; loop.next() )
  {
    // get the current combination
    Tracks tracks ( 2 ) ;
    loop.current ( tracks.begin() ) ;

    const LHCb::Track* first  = tracks[0] ;
    const LHCb::Track* second = tracks[1] ;

    // skip invalid
    if ( !first || !second ) { continue ; }              // CONTINUE
    // skip the same
    if ( first == second ) { continue ; }              // CONTINUE
    // reduce the double count :
    if ( same && !compare ( first , second ) ) { continue ; }         // CONTINUE

    // chech the overrlap
    if ( HltUtils::matchIDs ( *first , *second ) ) { continue ; }     // CONTINUE

    // apply the cuts on the combination of two tracks:
    if ( m_cut2tr && !(*m_cut2tr)( *first , *second ) ) { continue ;  }

    // create the vertex
    auto vertex = std::make_unique<LHCb::RecVertex>();

    /// fill it with some information
    creator ( *first , *second , *vertex ) ;

    // apply cuts on the vertex
    if ( m_cut4rv && !(*m_cut4rv)( *vertex ) ) { continue ; } // CONTINUE

    // good vertex! add it to the output container
    vertices.push_back ( vertex.release() ) ; // "vertex is not valid anymore

  }  // end of loop over all combinations
  //
  // return number of created vertcies
  return vertices.size() - size ; // RETURN
}
// ============================================================================
// The END
// ============================================================================

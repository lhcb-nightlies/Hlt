If you have a crucial bugfix or feature addition for the current production version, make a merge request with the 2017-patches branch. 

If you want to add a new line, make a merge request with the 2017-patches branch. 

If you have developments which are not needed for the 2017 production, make a merge request with master.

If you have developments for the upgrade, make a merge request with future.
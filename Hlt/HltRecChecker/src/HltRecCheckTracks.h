#ifndef HLTRECCHECKTRACKS_H
#define HLTRECCHECKTRACKS_H 1

// Include files
#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"
#include "Event/MCParticle.h"

/** @class HltRecCheckTracks HltRecCheckTracks.h
 *
 *
 *  @author Jose A. Hernando
 *  @date   2006-05-24
 */
class HltRecCheckTracks : public HltAlgorithm {
public:
  /// Standard constructor
  HltRecCheckTracks( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  Hlt::SelectionContainer<LHCb::Track,LHCb::Track> m_selections { *this };
  std::string m_linkName;

  AIDA::IHistogram1D* m_histoGhost = nullptr;
  AIDA::IHistogram1D* m_histoDx = nullptr;
  AIDA::IHistogram1D* m_histoDy = nullptr;
  AIDA::IHistogram1D* m_histoDz = nullptr;
  AIDA::IHistogram1D* m_histoDTx = nullptr;
  AIDA::IHistogram1D* m_histoDTy = nullptr;
  AIDA::IHistogram1D* m_histoPOP = nullptr;

};
#endif // HLTMCMONITOR_H

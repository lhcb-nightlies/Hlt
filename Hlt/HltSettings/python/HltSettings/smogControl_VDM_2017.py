from smogControl_2017 import smogControl_2017
from VanDerMeerScan_2017 import VanDerMeerScan_2017
from Utilities.Utilities import update_thresholds


class smogControl_VDM_2017(object):
    """Settings for combined  physics and VDM/BGI in 2017.

    @author R. Matev
    @date 2016-04-27
    @date 2016-06-08
    """

    def __init__(self):
        self.physics = smogControl_2017()
        self.lumi = VanDerMeerScan_2017()

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1608'

    def HltType(self):
        self.verifyType(smogControl_VDM_2017)
        return 'smogControl_VDM_2017'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = self.physics.ActiveHlt1Lines() + self.lumi.ActiveHlt1Lines()
        return list(set(lines))

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = self.physics.ActiveHlt2Lines() + self.lumi.ActiveHlt2Lines()
        return list(set(lines))

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = {}
        update_thresholds(thresholds, self.physics.Thresholds())
        update_thresholds(thresholds, self.lumi.Thresholds())

        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        update_thresholds(thresholds, {
            Hlt1BeamGasLinesConf: {
                'L0Filter' : {
                    'NoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'NoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'Beam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'Beam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BB'          : "L0_CHANNEL('CALOPhys')",
                },
                'Prescale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 0.001,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 0.001,
                },
            },
        })
        return thresholds

    def Streams(self):
        streams = {}
        streams.update(self.physics.Streams())
        streams.update(self.lumi.Streams())
        # TODO check that we're not overriding something
        return streams

    def NanoBanks(self):
        return self.lumi.NanoBanks()

    def StreamsWithLumi(self):
        streams = set(self.Streams().keys())
        streams = streams - set(['VELOCLOSING', 'EXPRESS'])
        return list(streams)

    def StreamsWithBanks(self):
        physics_banks = self.physics.StreamsWithBanks()
        lumi_banks = self.lumi.StreamsWithBanks()
        assert (not set(tuple(x[0]) for x in physics_banks)
                .intersection(tuple(x[0]) for x in lumi_banks))
        return physics_banks + lumi_banks

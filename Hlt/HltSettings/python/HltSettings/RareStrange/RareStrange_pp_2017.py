# =============================================================================
# @file   RareStrange_pp_2017.py
# @author Jessica Prisciandaro (jessica.prisciandaro@cern.ch)
# @date   12.05.2016
# =============================================================================
"""Threshold settings for Hlt2 Rare Strange lines for 2016."""

from GaudiKernel.SystemOfUnits import mm, MeV, picosecond


class RareStrange_pp_2017(object):
    __all__ = ('ActiveHlt2Lines', 'Thresholds')

    def ActiveHlt2Lines(self):
        """Returns a list of active lines."""

        lines = ['Hlt2RareStrangeSigmaPMuMu',
                 'Hlt2RareStrangeKPiMuMu',
                 'Hlt2RareStrangeKPiMuMuSS',
                 'Hlt2RareStrangeKsPiPiEETOS'
                 ]

        return lines

    def Thresholds(self):
        """Returns the line thresholds."""
        thresholds = {'Common': {'TrChi2': 3,
                                 'TrGP'  : 0.3},
                      'SigmaPMuMu': {'muonMinIpChi2' : 25.,
                                     'pPIDp'         : 5.,
                                     'pMinIpChi2'    : 25.,
                                     'SigmaMassWin'  : 500 * MeV,
                                     'SigmaMaxDOCA'  : 2. * mm,
                                     'SigmaVtxChi2'  : 25,   # adimensional
                                     'SigmaMinPt'    : 500 * MeV,
                                     'SigmaMinDIRA'  : 0.9,  # adimensional
                                     'SigmaMaxIpChi2': 36,   # adimensional
                                     'SigmaMinTauPs' : 6 * picosecond},
                      'KPiMuMu': {'muonMinIpChi2':  25.,
                                  'piMinIpChi2'  :  25.,
                                  'KMassWin'     : 500 * MeV,
                                  'KMaxDOCA'     : 2. * mm,
                                  'KVtxChi2'     : 25.,  # adimensional
                                  'KMinPt'       : 500 * MeV,
                                  'KMinDIRA'     : 0.9,  # adimensional
                                  'KMaxIpChi2'   : 36,   # adimensional
                                  'KMinTauPs'    : 10 * picosecond},
                      'KPiMuMuSS' : { 'muonMinIpChi2' :  25. ,
                                      'piMinIpChi2' :  25. ,
                                      'KMassWin' : 500 * MeV ,
                                      'KMaxDOCA' : 2. * mm ,
                                      'KVtxChi2' : 25. ,  # adimensional
                                      'KMinPt' : 500 * MeV ,
                                      'KMinDIRA' : 0.9 ,   # adimensional
                                      'KMaxIpChi2' : 36 ,  # adimensional
                                      'KMinTauPs'  : 10 * picosecond
                                      },
                      'KsPiPiEETOS' : {'L0Filter' : "L0_CHANNEL('Hadron') | L0_CHANNEL('Electron') | L0_CHANNEL('Muon')",
                                       'Hlt1Filter' : "HLT_PASS_RE('Hlt1((Two)?TrackMVA(Tight)?|TrackMuon|SingleMuonNoIP|DiMuonLowMass)Decision')",
                                       'TOSFilter'   : ['L0(Hadron|Electron|Muon)Decision%TOS',
                                                        'Hlt1((Two)?TrackMVA(Tight)?|TrackMuon|SingleMuonNoIP|DiMuonLowMass)Decision%TOS'],
                                       'piPT'          : 500 * MeV,
                                       'piMinIpChi2'   : 9 , # adimensional
                                       'KsMassMax'     : 550 * MeV,
                                       'KsMaxDOCA'     : 0.5 * mm,
                                       'KsVtxChi2Ndof' : 16, # adimensional
                                       'KsMinPt'       : 1500 * MeV,
                                       'KsMinDira'     : 0.9999, # adimensional
                                       'KsMaxIpChi2'   : 150, # adimensional
                                       'KsMaxIp'       : 0.3 * mm,
                                       'KsMinFDChi2'   : 150, # adimensional
                                       },

                      }
                      # Now build the final dictionary
        from Hlt2Lines.RareStrange.Lines import RareStrangeLines

        return {RareStrangeLines: thresholds}

# EOF


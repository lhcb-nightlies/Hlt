from GaudiKernel.SystemOfUnits import mm
from Configurables import HltConf


class VanDerMeerScan_2017(object):
    """Settings for VDM scans and BGI in 2017."""

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1620'

    def HltType(self):
        self.verifyType(VanDerMeerScan_2017)
        return 'VanDerMeerScan_2017'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # General lines
            'Hlt1Lumi',
            # 'Hlt1LumiSequencer',
            'Hlt1VeloClosingMicroBias',
            'Hlt1VeloClosingPV',
            # Beam-gas lines
            'Hlt1BeamGasNoBeamBeam1',
            'Hlt1BeamGasNoBeamBeam2',
            'Hlt1BeamGasBeam1',
            'Hlt1BeamGasBeam2',
            'Hlt1BeamGasCrossingForcedReco',
            'Hlt1BeamGasCrossingForcedRecoFullZ',
            'Hlt1BeamGasCrossingFullZLowNTracks',
            # Extra BeamGas lines
            'Hlt1BeamGasBeam1L0',
            'Hlt1BeamGasBeam1L0VeloOpen',
            'Hlt1BeamGasBeam1VeloOpen',
            'Hlt1BeamGasBeam2VeloOpen',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            'Hlt2Lumi',
            'Hlt2BeamGas',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf

        thresholds = {
            Hlt1LumiLinesConf: {
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 1,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "scale(L0_DECISION(LHCb.L0DUDecision.Any), RATE(10000))",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
            },
            Hlt1BeamGasLinesConf: {
                # Global behaviour settings
                'TrackingConf'          : 'FastVelo',
                'FitTracks'             : True,
                'PVFitter'              : 'LSAdaptPV3DFitter',
                'PVSeeding'             : 'PVSeed3DTool',
                'SplitVertices'         : True,
                'CreateGlobalSelection' : False,
                'Turbo'                 : False,
                'UseGEC'                : 'None',

                # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                'VertexMinNTracks'          : 9,  # strictly greater than
                'FullZVertexMinNTracks'     : 27,  # strictly greater than
                'Beam1VtxMaxBwdTracks'      : -1,  # less or equal than, negative to switch off
                'Beam2VtxMaxFwdTracks'      : -1,  # less or equal than, negative to switch off

                # z-ranges for Vertexing
                'Beam1VtxRangeLow'        : -2000.,
                'Beam1VtxRangeUp'         :  2000.,
                'Beam2VtxRangeLow'        : -2000.,
                'Beam2VtxRangeUp'         :  2000.,
                # Luminous region exclusion range
                'BGVtxExclRangeMin'       :  -250.,
                'BGVtxExclRangeMax'       :   250.,

                # Take any L0 channel
                'L0Filter' : {
                    'BeamGasNoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasNoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedReco': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedRecoFullZ': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingFullZLowNTracks': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam1L0': "L0_CHANNEL('B1gas')",
                    'BeamGasBeam1L0VeloOpen': "L0_CHANNEL('B1gas')",
                    'BeamGasBeam1VeloOpen': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam2VeloOpen': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                },

                'Prescale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                    'Hlt1BeamGasCrossingFullZLowNTracks' : 0.25,
                    'Hlt1BeamGasBeam1L0'                 : 0.01,
                    'Hlt1BeamGasBeam1L0VeloOpen'         : 1.,
                    'Hlt1BeamGasBeam1VeloOpen'           : 1.,
                    'Hlt1BeamGasBeam2VeloOpen'           : 1.,
                },
                'Postscale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                    'Hlt1BeamGasCrossingFullZLowNTracks' : 1.,
                    'Hlt1BeamGasBeam1L0'                 : 1.,
                    'Hlt1BeamGasBeam1L0VeloOpen'         : 1.,
                    'Hlt1BeamGasBeam1VeloOpen'           : 1.,
                    'Hlt1BeamGasBeam2VeloOpen'           : 1.,
                },
            },
        }

        # Hlt2 pass through lines
        from Hlt2Lines.Technical.Lines import TechnicalLines
        thresholds.update({
            TechnicalLines: {
                'Prescale': {
                    'Hlt2BeamGas': 1.,
                },
            }
        })
        return thresholds

    def Streams(self):
        return {
            # Deliberately turn off the LUMI stream to not double the large
            # rate of lumi during VDM (45 kHz) to storage
            'BEAMGAS': "HLT_PASS_SUBSTR('Hlt2BeamGas')",  # Hlt2Lumi is added by HltOutput
            'EXPRESS': None,
            'VELOCLOSING': None,
            'HLT1NOBIAS': None,

        }

    def NanoBanks(self):
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        nanobanks['BEAMGAS'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks

    def StreamsWithBanks(self):
        return [(["BEAMGAS"], 'KILL', [])]

    def StreamsWithLumi(self):
        return ['BEAMGAS']

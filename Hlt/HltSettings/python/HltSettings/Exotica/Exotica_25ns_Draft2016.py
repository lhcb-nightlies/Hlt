from GaudiKernel.SystemOfUnits import MeV, picosecond, mm

class Exotica_25ns_Draft2016:
    """
    Threshold settings for Hlt2 exotica lines for 2016 25 ns data-taking.

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author P. Ilten and M. Williams
    @date 2016-02-12
    """


    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """Returns a list of active lines."""

        lines = [
                 'Hlt2ExoticaDisplPhiPhi',
                 'Hlt2ExoticaDiMuonNoIPTurbo',
                 'Hlt2ExoticaQuadMuonNoIP',
                 'Hlt2ExoticaDisplDiMuon',
                 'Hlt2ExoticaDisplDiMuonNoPoint',
                 'Hlt2ExoticaPrmptDiMuonTurbo',
                 'Hlt2ExoticaPrmptDiMuonSSTurbo',
                 'Hlt2ExoticaPrmptDiMuonHighMass',
                 'Hlt2ExoticaRHNu',
                 'Hlt2ExoticaRHNuHighMass',
                 'Hlt2ExoticaDiRHNu',
                 #'Hlt2ExoticaD2PiEta_Eta2PiPiXee', # this line under development
                ]

        return lines
    
    def Thresholds(self) :
        """Return the trigger thresholds."""

        d = {}

        from Hlt2Lines.Exotica.Lines import ExoticaLines
        d.update( 
            { ExoticaLines : {
                'Prescale' : {'Hlt2ExoticaDiMuonNoIPTurbo': 0.01},
                'Common' : {'GhostProb' : 0.3},
                'ExoticaDisplPhiPhi' : {'TisTosSpec' : "Hlt1IncPhi.*Decision%TOS" ,
                                        'KPT'  : 500*MeV,
                                        'KIPChi2' : 16,
                                        'KProbNNk' : 0.1,
                                        'PhiPT'  : 1000*MeV,
                                        'PhiMassWindow' : 20*MeV,
                                        'VChi2' : 10,
                                        'FDChi2' : 45},
                'ExoticaSharedDiMuonNoIP' : {'MuPT' : 500*MeV,
                                             'MuP' : 10000*MeV,
                                             'MuProbNNmu' : 0.2,
                                             'DOCA' : 0.2*mm,
                                             'VChi2' : 10},
                'ExoticaDiMuonNoIPTurbo' : {'TisTosSpec' : None,
                                            'PT' : 1000*MeV},
                'ExoticaQuadMuonNoIP' : {'TisTosSpec' : None,
                                         'PT' : 0,
                                         'VChi2' : 10},       
                'ExoticaDisplDiMuon' : {'TisTosSpec' : None,
                                        'MuProbNNmu' : 0.5,
                                        'MuIPChi2' : 16,
                                        'PT' : 1000*MeV,                                   
                                        'IPChi2' : 16, 
                                        'FDChi2' : 30},
                'ExoticaDisplDiMuonNoPoint' : {'TisTosSpec' : None,
                                               'MuProbNNmu' : 0.8,
                                               'MuIPChi2' : 16,
                                               'PT' : 1000*MeV,                                   
                                               'FDChi2' : 30},
                'ExoticaPrmptDiMuonTurbo' : {'TisTosSpec' : "Hlt1DiMuon(HighMass)?(NoIP)?Decision%TOS",
                                             'MuPT' : 1000*MeV,
                                             'MuP' : 20000*MeV,
                                             'MuProbNNmu' : 0.95,
                                             'MuIPChi2' : 6,
                                             'PT' : 1000*MeV,                                   
                                             'FDChi2' : 45},
                'ExoticaPrmptDiMuonSSTurbo' : {'TisTosSpec' : "Hlt1DiMuon(HighMass)?(NoIP)?Decision%TOS",
                                               'MuPT' : 1000*MeV,
                                               'MuP' : 20000*MeV,
                                               'MuProbNNmu' : 0.95, 
                                               'MuIPChi2' : 6,
                                               'DOCA' : 0.2*mm,
                                               'VChi2' : 10,
                                               'PT' : 1000*MeV,                                   
                                               'FDChi2' : 45},
                'ExoticaPrmptDiMuonHighMass' : {'TisTosSpec' : "Hlt1DiMuonHighMassDecision%TOS",
                                                'MuPT' : 500*MeV,
                                                'MuP' : 10000*MeV,                                    
                                                'M'          : 3200*MeV,
                                                'MuProbNNmu' : 0.9, 
                                                'MuIPChi2' : 6,
                                                'PT' : 1000*MeV,                                   
                                                'FDChi2' : 45},
                'ExoticaSharedRHNu' : {'TisTosSpec' : None,
                                       'PT' : 500*MeV,
                                       'P' : 10000*MeV,                                              
                                       'ProbNNmu' : 0.5, 
                                       'IPChi2' : 16,
                                       'VChi2' : 10,
                                       'XIPChi2' : 16,
                                       'M' : 0,
                                       'TAU' : 1*picosecond,
                                       'FDChi2' : 45},
                'ExoticaRHNu' : {'TisTosSpec' : None,                 
                                 'M' : 0,
                                'TAU' : 1*picosecond},
                'ExoticaRHNuHighMass' : {'TisTosSpec' : None,
                                         'M' : 5000*MeV,
                                         'TAU' : 1*picosecond},
                'ExoticaDiRHNu' : {'TisTosSpec' : None,                 
                                   'PT' : 0,
                                   'VChi2' : 10},
                'ExoticaSharedDisplacedDiE' : {'EPT' : 200*MeV,
                                               'EP' : 2000*MeV,
                                               'EIPChi2' : 9,
                                               'EProbNNe' : 0.1,
                                               'M' : 0*MeV,
                                               'PT' : 0*MeV,                                   
                                               'FDChi2' : 30,
                                               'VChi2' : 100},
                'ExoticaSharedDisplacedEta2PiPiXee' :  {'PiPT' : 200*MeV,
                                                        'PiP' : 2000*MeV,
                                                        'PiIPChi2' : 9,
                                                        'PiPIDK' : 0,
                                                        'DM' : 200*MeV,
                                                        'PT' : 500*MeV,                                   
                                                        'FDChi2' : 30,
                                                        'VChi2' : 100},
                'ExoticaD2PiEta_Eta2PiPiXee' : {'TisTosSpec' : None,
                                                'PiPT' : 500*MeV,
                                                'PiP'  : 5000*MeV,
                                                'PiIPChi2' : 9,
                                                'PiPIDK' : 0,
                                                'PT' : 1000*MeV,
                                                'MMIN' : 1700*MeV,
                                                'MMAX' : 2100*MeV,
                                                'VChi2' : 10,
                                                'FDChi2' : 30}                                                 
            }
          }
        )

        return d

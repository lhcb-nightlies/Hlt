#ifndef HLTBASE_HLTSELECTIONCONTAINER_H
#define HLTBASE_HLTSELECTIONCONTAINER_H 1
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StringKey.h"
#include "HltBase/HltSelection.h"
#include "HltBase/HltAlgorithm.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/GaudiException.h"
#include "boost/utility.hpp"
#include <vector>
#include <utility>
#include <tuple>
#include <memory>
#include <cassert>
#include <map>
//
//  This file declares the templates SelectionContainerN<T1,T2,...TN> where
//  T1,T2, ..., TN are the type of candidate lists.
//
//  These classes are utilities intended to be member data of HltAlgorithms,
//  and will deal with setting up of input and output selections, and will
//  insure they contain valid data when input<N> and output are called.
//
//  The provide the following member functions:
//
//            void declareProperties()
//            void retrieveSelections()
//            void registerSelection()
//            TSelection<T1>* output()
//            TSelection<TN>* input<N>() for (N>1)
//
//  In an HltAlgorithm implementation, you should initialize the contained
//  container in the initializer list, passing it '*this', and call declareProperties
//  in the body of the constructor,  retrieveSelections & registerSelection during
// initialize, and
//  you can access the selections using output() and input<N>() during execute.
//

namespace Hlt {
namespace detail {

// -- walk through the tuple...
template <typename F, typename Tuple, size_t... I>
void map_impl( F& f, Tuple& t, std::index_sequence<I...> )
{
    (void)std::initializer_list<int>{ ( f( std::get<I>( t ), I ), 0 ) ... };
}

/// map functor f on each item in a tuple:
/// starting with f(get<0>), f(get<1>),... f(get<N-2>), f(get<N-1>)
template <typename F, typename... Args>
void map( F f, std::tuple<Args...>& t )
{
    map_impl( f, t, std::index_sequence_for<Args...>{} );
}

template <typename Candidate>
struct data_
{
    using selection_type = Hlt::TSelection<Candidate>;
    using candidate_type = Candidate;
    selection_type* selection = nullptr;
    Gaudi::StringKey property;
};

template <>
struct data_<void>
{
    using selection_type = Selection;
    using candidate_type = void;
    selection_type* selection = nullptr;
    Gaudi::StringKey property;
};

template <typename U>
inline void register_( data_<U>& d, HltAlgorithm& owner )
{
    d.selection = &owner.registerTSelection<U>( d.property );
}

template <>
inline void register_( data_<void>& d, HltAlgorithm& owner )
{
    d.selection = &owner.registerSelection( d.property );
}

template <typename Candidate>
struct cdata_
{
    using selection_type = const Hlt::TSelection<Candidate>;
    using candidate_type = Candidate;
    selection_type* selection = nullptr;
    Gaudi::StringKey property;
};

/// retrieve selection
class retrieve_
{
    HltAlgorithm& m_owner;
  public:
    retrieve_( HltAlgorithm& owner ) : m_owner( owner ) { }
    // specialization for output -- do nothing
    template <typename U> void operator()( detail::data_<U>&, size_t ) { }

    // specialization for input -- retrieve
    template <typename U> void operator()( detail::cdata_<U>& u, size_t )
    {
        if ( u.selection ) {
            throw GaudiException( m_owner.name() +
                                      "::retrieve_ selection already present..",
                                  "", StatusCode::FAILURE );
        }
        u.selection = &m_owner.retrieveTSelection<U>( u.property );
    }
};

//
/// declare property -- this fixes the naming convention for properties!
template <unsigned N>
class declare_
{
    HltAlgorithm& m_alg;
    std::map<int, std::string> m_defs;

  public:
    declare_( HltAlgorithm& alg, std::map<int, std::string>&& defaults )
        : m_alg( alg ),  m_defs{std::move( defaults )}
    { }
    template <typename U>
    void operator()( U& t , size_t i)
    {
        std::string def = m_defs[i];
        if ( i == 0 ) {
            t.property = Gaudi::StringKey{
                def.empty() ? m_alg.name() : def }; // set default output name...
            m_alg.declareProperty( "OutputSelection", t.property );
        } else {
            std::string prop{"InputSelection"};
            if ( N > 2 ) prop += std::to_string( i );
            if ( !def.empty() )
                t.property = Gaudi::StringKey( def );  // set default input name...
            m_alg.declareProperty( prop, t.property ); // TODO: add callback, locked
                                                       // as soon as corresponding
                                                       // TSelection* is non-zero...
                                                       // TODO: add help entry
        }
    }
};
}

template <typename T1, typename... Tn>
class SelectionContainer : private boost::noncopyable
{
    HltAlgorithm& m_owner; // The algorithm which owns us
    using Tuple = std::tuple<detail::data_<T1>, detail::cdata_<Tn>...>;
    Tuple m_data;

  public:
    using map_t = std::map<int, std::string>;

    SelectionContainer( HltAlgorithm& alg, map_t defaults = map_t{} ) : m_owner( alg )
    {
        constexpr auto N = std::tuple_size<Tuple>::value;
        map( detail::declare_<N>{ m_owner, std::move( defaults ) }, m_data );
    }
    // could move to postInitialize hook
    void registerSelection()
    {
        auto& d = std::get<0>( m_data );
        if ( d.selection ) {
            throw GaudiException(
                m_owner.name() + "::registerSelection selection already present..",
                "", StatusCode::FAILURE );
        }
        register_( d, m_owner );
    }

    // could move to preInitialize hook
    void retrieveSelections()
    { map( detail::retrieve_{m_owner}, m_data ); }

    // TODO: check if register/retrieve has been called...
    const auto* output() const
    { return std::get<0>( m_data ).selection; }

    // TODO: check if register/retrieve has been called...
    auto* output()
    { return std::get<0>( m_data ).selection; }

    // template <unsigned N, typename std::enable_if< (N>0) >::type*>
    template <unsigned N>
    typename std::tuple_element<N, Tuple>::type::selection_type* input()
    {
        static_assert(
            N > 0,
            "input() : N>0 for input -- N=0 corresponds to the required output" );
        auto& d = std::get<N>( m_data );
        if ( !d.selection->processed() ) {
            m_owner.debug() << "requesting stale selection " << d.selection->id()
                            << endmsg;
            m_owner.warning()
                << "requesting stale selection...  most likely a misconfiguration"
                << endmsg;
        }
        return d.selection;
    }
};
}

#endif

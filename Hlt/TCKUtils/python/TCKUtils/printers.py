import re
from functools import partial


class TextTreePrinter(object):
    _default_properties = [
        'RoutingBits', 'AcceptFraction', 'FilterDescriptor',
        'Preambulo', 'Code', 'InputLocations', 'Input', 'Inputs', 'TisTosSpecs',
        'DaughtersCuts', 'CombinationCut', 'MotherCut', 'DecayDescriptor',
        'OutputSelection', 'Output', 'DecayDescriptors', 'ReFitPVs',
        'BankTypes', 'KillFromAll', 'DefaultIsKill',
    ]
    _tab = 50

    def __init__(self, file, lines=None, properties=None):
        self._file = file
        self._lines = re.compile(lines) if lines is not None else None
        self._properties = properties or self._default_properties
        self._visited = set()

        self._trtable = {
            'Code': self.prettyPrintStreamer,
            'DaughtersCuts': partial(self.prettyPrintDict, fmt="'{0}' : '{1}'"),
            'DecayDescriptors' : self.prettyPrintList,
            'Inputs': self.prettyPrintList,
            'InputLocations': self.prettyPrintList,
            'Preambulo': self.prettyPrintList,
            'FilterDescriptor': partial(self.prettyPrintList, fmt="'{}'", skipEmpty=True),
            'RoutingBits': partial(self.prettyPrintDict, fmt='{0:2d} : "{1}"'),
        }

    @staticmethod
    def _len_last(s):
        """Return the length of the last line."""
        return len(s.rsplit('\n', 1)[-1])

    def prettyPrintStreamer(self, code):
        # code = code.replace('\n', '')
        offset = self._tab + 25 + 18
        return ('\n' + ' ' * offset + '>> ').join(i.strip() for i in code.split('>>'))

    def prettyPrintDict(self, code, fmt="{0!r} : {1!r}"):
        try:
            import ast
            d = ast.literal_eval(code)
            #d = eval(code)
            assert isinstance(d, dict)
            offset = self._tab + 25 + 18
            lines = (fmt.format(k, v) for k, v in d.iteritems())
            return "{ " + ('\n' + ' ' * offset + ', ').join(lines) + '\n' + ' ' * offset + "}"
        except SyntaxError:
            return code

    def prettyPrintList(self, code, fmt=None, skipEmpty=True):
        try:
            import ast
            l = ast.literal_eval(code)
            #l = eval(code)
            assert isinstance(l, list)
            if skipEmpty and not l:
                return ''
            if len(l) < 2:
                return code
            l = map(str, l)
            if fmt:
                l = (fmt.format(i) for i in l)
            offset = self._tab + 25 + 18
            # from pprint import pprint
            # import sys
            # pprint(l, sys.stderr)
            return '[ ' + ('\n' + offset * ' ' + ', ').join(l) + '\n' + offset * ' ' + ']'
        except SyntaxError:
            return code

    def prettyPrintLeaf(self, leaf, depth, only_name=False):
        line = depth * '   ' + leaf.name
        if self._len_last(line) > (self._tab - 1):
            line += '\n' + self._tab * ' '
        else:
            line += (self._tab - self._len_last(line)) * ' '
        line += '%-25.25s' % leaf.type
        if only_name:
            return line
        for k, v in leaf.props.iteritems():
            if k in self._properties and v:
                if self._tab + 25 < self._len_last(line):
                    line += '\n' + (self._tab + 25) * ' '
                if k in self._trtable.keys():
                    # import sys
                    # sys.stderr.write(leaf.fqn() + '\n' + k + '\n' + v + '\n')
                    prettyPrint = self._trtable.get(k)
                    if prettyPrint:
                        v = prettyPrint(v)
                if v:
                    line += '%-16s: %s' % (k, v)
        return line

    def pprint_tree(self, tree, depth=0):
        leaf = tree.leaf
        visited = leaf in self._visited

        if leaf and leaf.kind == 'IAlgorithm':
            if self._lines and leaf.type in ['Hlt::Line', 'HltLine']:
                if not re.match(self._lines, leaf.name):
                    return
            line = self.prettyPrintLeaf(leaf, depth, only_name=visited)
            if visited:
                line += "...(repeats)"
            self._file.write(line + '\n')

        if not visited:
            self._visited.add(leaf)
            for i in tree.nodes:
                self.pprint_tree(i, depth + 1)

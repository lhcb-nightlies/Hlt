from copy import deepcopy
from Hlt2Combiner  import Hlt2Combiner
from Configurables import CombineParticles

class Hlt2ExtraCombiner(Hlt2Combiner):
    """
    Designed to allow user to select what particles in the tree to save
    Takes two decay descriptors as arguments:
        first  -  two combine new candidate (similar as for Hlt2Combiner)
        second -  to specify which particles in the tree are to be saved

    First decay descriptor of Hlt2ExtraCombiner is passed to normal CombineParticles,
    so it should follow these rules:
        - only fully described 1-step decay
        - only true particle names can appear in the descriptor
        - only "simple" arrow "->" can be used
        -  [...]cc (in lower case!)

    Second decay descriptor is passed to FilterDecays and is much less constrained:
        - use ^ to specify particles, that should be saved
        - use [...]CC in upper case
    """
    def __init__(self, name, decay, inputs, dependencies = [], tistos = [],
                 combiner = CombineParticles, nickname = None, shared = False, ParticlesToSave = None, **kwargs):
        self.__shared = shared
        self.__ParticlesToSave = ParticlesToSave # allows to save only selectes branch of decay (for CombineParticles)

        super(Hlt2ExtraCombiner, self).__init__(name, decay, inputs, dependencies,
                                                tistos, combiner, nickname, shared, **kwargs)

    def _makeMember(self, stages, cuts, args):

        # Create normal CombineParticles algorithm
        cp = Hlt2Combiner._makeMember(self, stages, cuts, args)
        stages.append(cp)

        from HltLine.HltLine import Hlt2Member
        from Configurables   import FilterDecays

        # Create FilterDecays to simulate 'ParticlesToSave' property
        # takes output of CombineParticles as input
        pts = Hlt2Member(FilterDecays, self._name() + 'ParticlesToSave',
                         shared = self.__shared, Code = self.__ParticlesToSave, Inputs = [cp])
        return pts

    def _clone_arguments(self, kwargs):
        """Return the updated arguments to construct a new instance."""
        ParticlesToSave = kwargs.pop('ParticlesToSave', self.__ParticlesToSave)
        args = super(Hlt2ExtraCombiner, self)._clone_arguments(kwargs)
        args['ParticlesToSave'] = ParticlesToSave
        return args

    def clone(self, name, **kwargs):
        return Hlt2ExtraCombiner(name, **(self._clone_arguments(kwargs)))

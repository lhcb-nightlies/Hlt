import re
import random
import string
from Gaudi.Configuration import log
from Configurables import FilterDesktop, CombineParticles
from Configurables import (DaVinci__N3BodyDecays, DaVinci__N4BodyDecays,
                           DaVinci__N5BodyDecays, DaVinci__N6BodyDecays,
                           DaVinci__N7BodyDecays)
from HltLine.HltLine import Hlt2Member

# Properties containing cuts
cutProps = ['CombinationCut', 'MotherCut', 'Code']

# the types of algorithms we care about
nbodyCombiners = [DaVinci__N3BodyDecays, DaVinci__N4BodyDecays,
                  DaVinci__N5BodyDecays, DaVinci__N6BodyDecays,
                  DaVinci__N7BodyDecays]
combinerTypes = [CombineParticles] + nbodyCombiners

# Masses obtained from the particle table rounded to the nearest
# MeV. Ranges are 5% left and right of mass
particleRanges = {'B+': (5279.0, 5015.0, 5543.0),
                  'B0': (5280.0, 5016.0, 5544.0),
                  'B_c+': (6274.0, 5960.0, 6588.0),
                  'B_s0': (5367.0, 5099.0, 5635.0),
                  'CLUSjet': (),
                  'D*(2010)+': (2010.0, 1909.0, 2111.0),
                  'D+': (1870.0, 1776.0, 1964.0),
                  'D0': (1865.0, 1772.0, 1958.0),
                  'D_s+': (1968.0, 1870.0, 2066.0),
                  'H_10': (125700.0, 119415.0, 131985.0),
                  'J_psi(1S)': (3097.0, 2942.0, 3252.0),
                  'K*(892)0': (896.0, 851.0, 941.0),
                  'K+': (494.0, 469.0, 519.0),
                  'KS0': (498.0, 473.0, 523.0),
                  'Lambda0': (1116.0, 1060.0, 1172.0),
                  'Lambda_b0': (5620.0, 5339.0, 5901.0),
                  'Lambda_c+': (2286.0, 2172.0, 2400.0),
                  'Omega_b~+': (6049.0, 5747.0, 6351.0),
                  'Omega~+': (1672.0, 1588.0, 1756.0),
                  'Sigma+': (1189.0, 1130.0, 1248.0),
                  'Sigma_c0': (2454.0, 2331.0, 2577.0),
                  'Upsilon(1S)': (9460.0, 8987.0, 9933.0),
                  'Xi_b~+': (5795.0, 5505.0, 6085.0),
                  'Xi_c0': (2471.0, 2347.0, 2595.0),
                  'Xi_cc+': (3598.0, 3418.0, 3778.0),
                  'Xi_cc++': (3598.0, 3418.0, 3778.0),
                  'Xi~+': (1322.0, 1256.0, 1388.0),
                  'chi_b0(1P)': (9859.0, 9366.0, 10352.0),
                  'chi_b1(1P)': (9893.0, 9398.0, 10388.0),
                  'chi_c1(1P)': (3511.0, 3335.0, 3687.0),
                  'phi(1020)': (1019.0, 968.0, 1070.0),
                  'psi(3770)': (3773.0, 3584.0, 3962.0),
                  'tau+': (1777.0, 1688.0, 1866.0)}

# Find the mother particle in a decay descriptor
re_descriptor = re.compile(r'\s*\[?\s*(\S+)\s*->(.*)')

# Search for ADMASS functors
re_dmass = re.compile(r"(ADA?MASS)\s*\(\s*'([^']*)'\s*\)\s*"
                      "<\s*(\d+\.?(?:\d+)?)\s*\*?\s*([MG]eV)?")

# Search for BPVCORRM, AM, M or MM functors, separte for functor is
# righthand argument and functor is lefthand operand.
re_rmass = re.compile(r'(\d+\.?(?:\d+)?)\s*\*?\s*([MG]eV)?'
                      '\s*(=?(?:>|<)=?)\s*((?:BPVCORR|A)?MM?)')
re_lmass = re.compile(r'((?:BPVCORR|A)?MM?)\s*(=?(?:>|<)=?)'
                      '\s*(\d+\.?(?:\d+)?)\s*\*?\s*([MG]eV)?')

# Search for in_range(l, {BPVCORRM, AM, M ,MM}, r)
re_range = re.compile(r'in_range\s*\('
                      '\s*(\d+\.?(?:\d+)?)\s*\*?\s*([MG]eV)?\s*,'
                      '\s*((?:BPVCORR|A)?MM?)\s*,'
                      '\s*(\d+\.?(?:\d+)?)\s*\*?\s*([MG]eV)?\s*\)')


def normalizeName(mother):
    """ Normalize the name of mother particles by replacing - with +
    and removing ~ """
    rep = mother.replace('-', '+')
    if '+' in rep:
        if rep != mother:
            # Originally negative particle, add ~ when making anti-particle
            rep = rep.replace('+', '~+')
    else:
        # Neutral particle
        rep = rep.replace('~', '')
    rep = rep.replace('/', '_')
    return rep


def idGenerator(mother, size=6, chars=string.ascii_uppercase + string.digits):
    """Generate a random string of a given size seeded with the name of
    the mother without disturbing the state of the random generator."""
    state = random.getstate()
    random.seed(hash(mother))
    id = ''.join(random.choice(chars) for _ in range(size))
    random.setstate(state)
    return id


def getMothers(combiner):
    """ Find all mother particles created by a combiner """
    mothers = set()
    if isinstance(combiner, Hlt2Member):
        descriptors = combiner.Args.get('DecayDescriptors', [])[:]
        desc = combiner.Args.get('DecayDescriptor', None)
        if desc:
            descriptors += [desc]
        name = combiner.name('')
    else:
        descriptors = combiner.DecayDescriptors[:]
        if combiner.DecayDescriptor:
            descriptors += [combiner.DecayDescriptor]
        name = combiner.getName()
    if not descriptors:
        print 'No decay descriptors found for combiner', name
        return mothers
    for desc in descriptors:
        m = re_descriptor.match(desc)
        if not m:
            log.warning("Couldn't match decay descriptor %s", desc)
            continue
        p = normalizeName(m.group(1))
        r = particleRanges.get(p, ())
        if r:
            mothers.add(p)
        else:
            log.debug("Skipping default mass plots for mother %s", p)
    return mothers


def getRange(mothers, algos):
    def scaleFactor(u):
        return 1. if not u or u == 'MeV' else 1000.

    def tolerance(f):
        if f == 'BPVCORRM':
            return 0.2
        elif f == 'AM' or f == 'ADAMASS':
            return 0.1
        else:
            return 0.

    upper = []
    lower = []

    for alg in algos:
        cuts = {}
        if type(alg) == Hlt2Member:
            cuts = [v for k, v in alg.Args.iteritems() if k in cutProps]
        else:
            cuts = [getattr(alg, k) for k in cutProps if hasattr(alg, k)]

        for cut in cuts:
            cut = cut.replace('\n', '')
            # ADMASS
            m = re_dmass.findall(cut)
            for fun, mother, v, u in m:
                m = normalizeName(mother)
                mass = particleRanges[m][0]
                l = mass - float(v) * scaleFactor(u)
                r = mass + float(v) * scaleFactor(u)
                lower.append(l - l * tolerance(fun))
                upper.append(r + r * tolerance(fun))

            # in_range
            m = re_range.findall(cut)
            for l, lu, f, r, ru in m:
                l = float(l) * scaleFactor(lu)
                r = float(r) * scaleFactor(ru)
                lower.append(l - l * tolerance(f))
                upper.append(r + r * tolerance(f))

            # Single sided mass cut, functor on the right
            m = re_rmass.findall(cut)
            for v, u, e, f in m:
                v = float(v) * scaleFactor(u)
                if e == '<':
                    lower.append(v - v * tolerance(f))
                else:
                    upper.append(v + v * tolerance(f))

            # Single sided mass cut, functor on the left
            m = re_lmass.findall(cut)
            for f, e, v, u in m:
                v = float(v) * scaleFactor(u)
                if e == '<':
                    upper.append(v + v * tolerance(f))
                else:
                    lower.append(v - v * tolerance(f))

    if not lower:
        lower = [particleRanges[mother][1] for mother in mothers]
    if not upper:
        upper = [particleRanges[mother][2] for mother in mothers]
    # Select the tightest cuts that are applied
    return max(lower), min(upper)


def getMonMembers(algos):
    """Get a list of algorithms that are filters, including the last
       combiner
    """
    def algType(m, types):
        return ((isinstance(m, Hlt2Member) and m.Type in types)
                or type(m) in types)

    # Get everything of the types we're interested in
    members = filter(lambda m: algType(m, combinerTypes + [FilterDesktop]),
                     algos)
    # Return the last combiner and all filter after it
    try:
        i = max(i for i, m in enumerate(members) if algType(m, combinerTypes))
        return members[i:]
    except ValueError:
        return []


def nbins(low, high):
    return int(max(min((high - low) / 2., 200), 50))


def ranges(mothers, algos):
    """ Get the plot ranges for all mother particles """
    def checkRange(min_mass, max_mass, bins, low, high):
        msg = ''
        if low >= min_mass:
            low = round(min_mass - 0.1 * min_mass)
            msg += 'Lower edge >= min mass; set to %6.0f. ' % low
        if high <= max_mass:
            high = round(max_mass + 0.1 * max_mass)
            msg += 'High edge <= max mass; set to %6.0f. ' % high
        if low >= high:
            high = round(max_mass + 0.1 * max_mass)
            msg += 'Upper edge <= lower edge; set to %6.0f. ' % high
        if bins <= 0:
            msg += 'Bins <= 0. Set to 50 '
            bins = nbins(low, high)
        return bins, low, high, msg

    # Get the lower and upper bounds
    low, high = getRange(mothers, algos)
    # 2 MeV bins if it's between 50 and 200, otherwise either of those, as
    # applicable
    bins = nbins(low, high)

    if len(mothers) > 1:
        # Make an overlap histogram that combines ranges, and then a
        # single one per mother particle.
        min_mass = min(particleRanges[n][0] for n in mothers)
        max_mass = max(particleRanges[n][0] for n in mothers)
        bins, low, high, msg = checkRange(min_mass, max_mass, low, high, bins)

        rngs = {'overlap': (bins, low, high, msg)}
        for n in mothers:
            _, low, high = particleRanges[n]
            rngs[n] = (nbins(low, high), low, high, msg)
        return rngs
    else:
        n = list(mothers)[0]
        mass = particleRanges[n][0]
        bins, low, high, msg = checkRange(mass, mass, bins, low, high)
        return {n: (bins, low, high, msg)}


def preambulo(mothers, algos):
    """Generate the preambulo needed for monitoring.

    All histograms get a name that includes a random string
    initialized with the hash of the mother particle.
    """
    errorMsg = ''
    pre = []
    histos = {}
    rngs = ranges(mothers, algos)
    for mother, (bins, low, high, msg) in rngs.iteritems():
        errorMsg += msg
        mother = normalizeName(mother)
        # Gaudi histogram argument order is (low, high, n_bins)
        histo = 'massHisto_' + idGenerator(mother)
        histos[mother] = (histo, 'M')
        pre += ["%s = Gaudi.Histo1DDef('%s_mass', %d, %d, %d)"
                % (histo, mother, low, high, bins)]

    # Try to monitor D* - D0 mass difference and D0 mass if the mother is s D*
    if 'D*(2010)+' in mothers:
        low_D0, high_D0 = particleRanges['D0'][1:]
        nbins_D0 = nbins(low_D0, high_D0)
        for title, fun, low, high, nb in [
                ('Dst_D0_delta', "(M - CHILD(M, ABSID == 'D0'))", 141, 153, 48),
                ('D0', "CHILD(M, ABSID == 'D0')", low_D0, high_D0, nbins_D0)]:
            histo = 'massHisto_' + idGenerator(title)
            histos[title] = (histo, fun)
            pre += ["%s = Gaudi.Histo1DDef('%s_mass', %d, %d, %d)"
                    % (histo, title, low, high, nb)]
    return pre, histos, errorMsg


def streamer(histos):
    """ histos = {mother : histo_name} """
    functors = []
    for title, (histo_name, fun) in histos.iteritems():
        functors += ["process(monitor(%s, %s, '%s_mass'))" %
                     (fun, histo_name, title)]
    return ' >> '.join(functors) + ' >> ~EMPTY'

from Utilities import allCuts
from HltLine.HltLine import Hlt2Member
from HltLine.HltLinesConfigurableUser import HltLinesConfigurableUser
from Gaudi.Configuration import log
from HltTracking.Hlt2AllTrackings import Hlt2AllTrackings
from Configurables import HltJetConf
from Configurables import FilterDesktop
from Hlt2Monitoring import getMonMembers, getMothers, preambulo, streamer
from Hlt2Lines.Utilities.Utilities import uniqueEverseen


def _flatten(l):
    from HltLine.HltLine import bindMembers
    for i in l:
        if isinstance(i, bindMembers):
            for j in _flatten(i.members()):
                yield j
        else:
            yield i

class Hlt2LinesConfigurableUser(HltLinesConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2AllTrackings,
        HltJetConf,
    ]

    __slots__ = {
        '_stages': {},
        '_intermediateStages': {},
        '_relatedInfo': {},
        'Prescale': {},
        'Postscale': {},
    }

    def initialize(self):
        # ensure queried configurables are instantiated and enabled
        # whenever self is instantiated and enabled
        if self._enabled:
            for Conf in self.__queried_configurables__:
                Conf()
        # TODO do we actually need this? Seems too late, and all the neccessary
        # magic happens in Hlt2Conf

    def relatedInfo(self):
        return {}

    def extraAlgos(self):
        return {}

    def __stages(self, source):
        # External stage sets the configurable to something that is not self.
        if source._configurable():
            conf = source._configurable()
        else:
            conf = self

        cuts = allCuts(conf)
        deps = []
        stage = source.stage(deps, cuts)
        if stage in deps:
            print 'WARNING: Circular dependency %s %s %s.' % (source._name(),
                                                              source._deps(),
                                                              source._inputs())
        stages = filter(lambda i: bool(i), deps + [stage])
        return stages

    def __relatedInfoSequence(self, stages, algorithms):
        if not stages:
            return ([], stages)

        members = []
        locations = []
        for stage in stages:
            # External stage sets the configurable to something that is not
            # self.
            if stage._configurable():
                conf = stage._configurable()
            else:
                conf = self
            cuts = allCuts(conf)
            infoStage, locs = stage.infoStage(cuts, algorithms[-1])
            members.append(infoStage)
            locations.extend(locs)

        # Consistency check
        if len(locations) != len(set(locations)):
            raise Exception(
                'At least one location in %s appears twice, this will not work!' % locations)

        from HltLine.HltLine import Hlt2SubSequence
        return (locations, [Hlt2SubSequence("RelatedInfoSequence", members,
                                            ModeOR=True, ShortCircuit=False)])

    def __extraAlgosSequence(self, stages):
        from Hlt2Stage import Hlt2Stage

        sd = {}
        for name, stg in stages.iteritems():
            sl = []
            for i in stg:
                if isinstance(i, Hlt2Stage):
                    sl.extend(self.__stages(i))
                else:
                    sl.append(i)
            sd[name] = list(uniqueEverseen(_flatten(sl)))
        return sd


    def algorithms(self, stages):
        from Hlt2Stage import Hlt2Stage

        # Loop over the dictionary of {linename : [stages]} that we've
        # been given.
        for name, stg in stages.iteritems():
            sl = []
            for i in stg:
                # If the stage is an Hlt2Stage use it's machinery,
                # otherwise add it directly.
                if isinstance(i, Hlt2Stage):
                    sl.extend(self.__stages(i))
                else:
                    sl.append(i)

            # Flatten the resulting sequence and remove any duplicates.
            allMembers = list(uniqueEverseen(_flatten(sl)))

            # We have a list of Hlt2Member here, add some default mass plots.
            # First find the last combiner and successive filters
            members = getMonMembers(allMembers)
            if not members:
                log.debug('Could not find monitoring candidate '
                          'algorithms for %s', name)
            mothers = set()
            if members:
                mothers = getMothers(members[0])

            # Format the preambulo and streamer if the monitoring
            # properties are empty, we only check members of this line
            # (which are Hlt2Member), and skip configurables.
            hlt2Members = filter(lambda m: isinstance(m, Hlt2Member), members)
            if mothers and not any('MotherMonitor' in m.Args
                                   or 'PostMonitor' in m.Args
                                   for m in hlt2Members):
                # Do default monitoring
                pre, histos, error = preambulo(mothers, members)
                if error:
                    log.debug('%s %s', name, error)
                fun = streamer(histos)
                lastMember = members[-1]
                if not isinstance(lastMember, Hlt2Member):
                    log.debug("Not adding default mass plots to %s, as monitoring "
                              "properties are already set." % name)
                else:
                    # Update an existing preambulo by adding our lines.
                    # and add the streamer to the correct property.
                    lastMember.Args['Preambulo'] = lastMember.Args.get('Preambulo', []) + pre
                    if lastMember.Type == FilterDesktop:
                        lastMember.Args['PostMonitor'] = fun
                    else:
                        lastMember.Args['MotherMonitor'] = fun

            # Return a tuple of at least (linename, algorithms), including
            # related info and/or extra algorithms, if they're present
            # FIXME this isn't nice, as the third return value can be different
            # depending on relInfo/extraAlgs status, maybe use namedtuple?
            result = [name, allMembers]
            ri = self.relatedInfo()
            ea = self.extraAlgos()
            if ri:
                riSeq = self.__relatedInfoSequence(ri.get(name, []), allMembers)
                result.append(riSeq)
            if ea:
                eaSeq = self.__extraAlgosSequence(ea.get(name, {}))
                result.append(eaSeq)

            yield tuple(result)

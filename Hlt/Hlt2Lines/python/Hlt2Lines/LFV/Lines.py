""" Hlt2 trigger lines for lepton flavour violating decays.

    Authors: Johannes Albrecht <johannes.albrecht@cern.ch>
             Kevin Dungs <kevin.dungs@cern.ch>
             Stefanie Reichert <stefanie.reichert@cern.ch>
"""

from GaudiKernel.SystemOfUnits import MeV
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
from HltLine.HltLine import Hlt2Line


class LFVLines(Hlt2LinesConfigurableUser):
    m_jpsi = 3096
    m_phi  = 1020

    __slots__ = {'Prescale': {},
                 'PromptPrescale': 1.0,
                 'Common': {},
                 'JpsiMuE'     : {'CombMassHigh': (m_jpsi + 550) * MeV,
                                  'CombMassLow': (m_jpsi - 1100) * MeV,
                                  'ElectronProbNn': 0.93,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.2,
                                  'MassHigh': (m_jpsi + 500) * MeV,
                                  'MassLow': (m_jpsi - 1000) * MeV,
                                  'MuonProbNn': 0.9,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.1,
                                  'VertexChi2DoF': 3},
                 'PhiMuE'      : {'CombMassHigh': (m_phi + 550) * MeV,
                                  'CombMassLow': (m_phi - 850) * MeV,
                                  'ElectronProbNn': 0.97,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.2, 
                                  'MassHigh': (m_phi + 500) * MeV,
                                  'MassLow': (m_phi - 800) * MeV,
                                  'MuonProbNn': 0.95,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.15,
                                  'VertexChi2DoF': 3,
                                  'IPCHI2_Min': 16,
                                  'BPVVDCHI2_Min': 100},
                 'PromptPhiMuE' : {'CombMassHigh': (m_phi + 550) * MeV,
                                  'CombMassLow': (m_phi - 850) * MeV,
                                  'ElectronProbNn': 0.97,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.2, 
                                  'MassHigh': (m_phi + 500) * MeV,
                                  'MassLow': (m_phi - 800) * MeV,
                                  'MuonProbNn': 0.95,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.15,
                                  'VertexChi2DoF': 3},
                 'UpsilonMuE'  : {'CombMassHigh': (13000) * MeV,
                                  'CombMassLow': (7000) * MeV,
                                  'ElectronProbNn': 0.2,
                                  'ElectronTrChi2DoF': 3,
                                  'ElectronTrGhostProb': 0.3,
                                  'MassHigh': (13000) * MeV,
                                  'MassLow': (7000) * MeV,
                                  'MuonProbNn': 0.2,
                                  'MuonTrChi2DoF': 3,
                                  'MuonTrGhostProb': 0.2,
                                  'VertexChi2DoF': 3},
                 'SpdCut'      : {'NSPD': 200},
                 'PromptSpdCut': {'NSPD': 100},}

    def __apply_configuration__(self):

        from Stages import SelJpsiMuE, SelPhiMuE, SelPromptPhiMuE, SelUpsilonMuE
        stages = {'LFVJpsiMuETurbo': [SelJpsiMuE],
                  'LFVPhiMuETurbo': [SelPhiMuE],
                  'LFVPromptPhiMuETurbo': [SelPromptPhiMuE],
                  'LFVUpsilonMuETurbo': [SelUpsilonMuE]
                 }

        for name, algos in self.algorithms(stages):
            if 'UpsilonMuE' in name:
                Hlt2Line(name,
                         prescale=self.prescale,
                         algos=algos,
                         postscale=self.postscale,
                         Turbo=True)
            elif 'Prompt' in name:
                Hlt2Line(name,
                         L0DU="(L0_DATA('Spd(Mult)') < {})".format(self.getProp('PromptSpdCut')['NSPD']),
                         prescale=self.getProp('PromptPrescale'),
                         algos=algos,
                         postscale=self.postscale,
                         Turbo=True)
            else: 
                Hlt2Line(name,
                         L0DU="(L0_DATA('Spd(Mult)') < {})".format(self.getProp('SpdCut')['NSPD']),
                         prescale=self.prescale,
                         algos=algos,
                         postscale=self.postscale,
                         Turbo=True)


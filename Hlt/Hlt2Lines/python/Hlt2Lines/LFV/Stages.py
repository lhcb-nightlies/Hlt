from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Inputs import (Hlt2Muons, Hlt2Electrons)


class JpsiMuECombiner(Hlt2Combiner):
    def __init__(self, name):
        def daughter_cut(particle_short, particle_long):
            return """(TRCHI2DOF < %({l}TrChi2DoF)s)
                    & (TRGHOSTPROB < %({l}TrGhostProb)s)
                    & (PROBNN{s} > %({l}ProbNn)s)""".format(s=particle_short,
                                                            l=particle_long)

        def asymmetric_mass_cut(functor, varname):
            return "({f} > %({v}Low)s) & ({f} < %({v}High)s)".format(f=functor,
                                                                     v=varname)

        dc = {'mu+': daughter_cut('mu', 'Muon'),
              'e-': daughter_cut('e', 'Electron')}
        cc = asymmetric_mass_cut('AM', 'CombMass')
        mc = (asymmetric_mass_cut('M', 'Mass') +
              " & (CHI2VXNDOF < %(VertexChi2DoF)s)")

        Hlt2Combiner.__init__(self,
                              'JpsiMuE',
                              '[J/psi(1S) -> mu+ e-]cc',
                              inputs=[Hlt2Muons, Hlt2Electrons],
                              DaughtersCuts=dc,
                              CombinationCut=cc,
                              MotherCut=mc,
                              Preambulo=[])

class PhiMuECombiner(Hlt2Combiner):
    def __init__(self, name):
        def daughter_cut(particle_short, particle_long):
            return """(TRCHI2DOF < %({l}TrChi2DoF)s)
                    & (TRGHOSTPROB < %({l}TrGhostProb)s)
                    & (BPVIPCHI2() > %(IPCHI2_Min)s )
                    & (PROBNN{s} > %({l}ProbNn)s)""".format(s=particle_short,
                                                            l=particle_long)

        def asymmetric_mass_cut(functor, varname):
            return "({f} > %({v}Low)s) & ({f} < %({v}High)s)".format(f=functor,
                                                                     v=varname)

        dc = {'mu+': daughter_cut('mu', 'Muon'),
              'e-': daughter_cut('e', 'Electron')}
        cc = asymmetric_mass_cut('AM', 'CombMass')
        mc = (asymmetric_mass_cut('M', 'Mass') +
              " & (BPVVDCHI2 > %(BPVVDCHI2_Min)s )" +
              " & (CHI2VXNDOF < %(VertexChi2DoF)s) ")

        from HltTracking.HltPVs import PV3D

        Hlt2Combiner.__init__(self,
                              'PhiMuE',
                              '[phi(1020) -> mu+ e-]cc',
                              inputs=[Hlt2Muons, Hlt2Electrons],
                              dependencies = [ PV3D('Hlt2') ],
                              DaughtersCuts=dc,
                              CombinationCut=cc,
                              MotherCut=mc,
                              Preambulo=[])

class PromptPhiMuECombiner(Hlt2Combiner):
    def __init__(self, name):
        def daughter_cut(particle_short, particle_long):
            return """(TRCHI2DOF < %({l}TrChi2DoF)s)
                    & (TRGHOSTPROB < %({l}TrGhostProb)s)
                    & (PROBNN{s} > %({l}ProbNn)s)""".format(s=particle_short,
                                                            l=particle_long)

        def asymmetric_mass_cut(functor, varname):
            return "({f} > %({v}Low)s) & ({f} < %({v}High)s)".format(f=functor,
                                                                     v=varname)

        dc = {'mu+': daughter_cut('mu', 'Muon'),
              'e-': daughter_cut('e', 'Electron')}
        cc = asymmetric_mass_cut('AM', 'CombMass')
        mc = (asymmetric_mass_cut('M', 'Mass') +
              " & (CHI2VXNDOF < %(VertexChi2DoF)s) ")

        from HltTracking.HltPVs import PV3D

        Hlt2Combiner.__init__(self,
                              'PromptPhiMuE',
                              '[phi(1020) -> mu+ e-]cc',
                              inputs=[Hlt2Muons, Hlt2Electrons],
                              dependencies = [ PV3D('Hlt2') ],
                              DaughtersCuts=dc,
                              CombinationCut=cc,
                              MotherCut=mc,
                              Preambulo=[])

class UpsilonMuECombiner(Hlt2Combiner):
    def __init__(self, name):
        def daughter_cut(particle_short, particle_long):
            return """(TRCHI2DOF < %({l}TrChi2DoF)s)
                    & (TRGHOSTPROB < %({l}TrGhostProb)s)
                    & (PROBNN{s} > %({l}ProbNn)s)""".format(s=particle_short,
                                                            l=particle_long)

        def asymmetric_mass_cut(functor, varname):
            return "({f} > %({v}Low)s) & ({f} < %({v}High)s)".format(f=functor,
                                                                     v=varname)

        dc = {'mu+': daughter_cut('mu', 'Muon'),
              'e-': daughter_cut('e', 'Electron')}
        cc = asymmetric_mass_cut('AM', 'CombMass')
        mc = (asymmetric_mass_cut('M', 'Mass') +
              " & (CHI2VXNDOF < %(VertexChi2DoF)s)")

        decays_Upsilon = [ 
             '[Upsilon(1S) -> mu+ e-]cc',
             '[Upsilon(2S) -> mu+ e-]cc',
             '[Upsilon(3S) -> mu+ e-]cc',
        ]

        Hlt2Combiner.__init__(self,
                              'UpsilonMuE',
                              decays_Upsilon,
                              inputs=[Hlt2Muons, Hlt2Electrons],
                              DaughtersCuts=dc,
                              CombinationCut=cc,
                              MotherCut=mc,
                              Preambulo=[])

SelJpsiMuE       = JpsiMuECombiner('JpsiMuE')
SelPhiMuE        = PhiMuECombiner('PhiMuE')
SelPromptPhiMuE  = PromptPhiMuECombiner('PromptPhiMuE')
SelUpsilonMuE    = UpsilonMuECombiner('UpsilonMuE')

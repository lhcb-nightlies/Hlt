from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Inputs import Hlt2LoosePions, Hlt2LooseKaons, Hlt2LooseMuons,Hlt2NoPIDPions

class SLB_D02HH(Hlt2Combiner) : 
    def __init__(self, name, decay, inputs, nickname = None, shared = False) : 
        
        dc = { }
        children = []
        for d in decay:
            if d.find("K-") !=-1 or d.find("K+") != -1:
                children += ["K+"]
            if d.find("pi-") !=-1 or d.find("pi+") != -1:
                children += ["pi+"]
        for child in children :
            dc[child] = ( "(PT > %(Had_PT_MIN)s)" +
                          "& (P > %(Had_P_MIN)s)" +
                          "& (MIPCHI2DV(PRIMARY) > %(Had_MIPCHI2DV_MIN)s)"+
                          "& (TRGHOSTPROB < %(Had_GHOSTPROB_MAX)s)" )
            if child == 'pi+':
                dc["pi+"] = dc["pi+"] + "& (PIDK < %(Pi_PIDK_MAX)s)"
            if child == 'K+':
                dc["K+"] = dc["K+"] + "& (PIDK > %(K_PIDK_MIN)s)"
                

        combCut = ( "(ADAMASS('D0') < %(D0_AMassWin)s)" )

        parentCut = ( "(VFASPF(VCHI2PDOF) < %(D0_VCHI2PDOF_MAX)s)" +
                      "& (BPVVDCHI2 > %(D0_BPVVDCHI2_MIN)s)" +
                      "& (ADMASS('D0')  < %(D0_MassWin)s)" )

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__( self, name, decay, inputs,
                               dependencies = [PV3D('Hlt2')],
                               nickname = nickname,
                               shared = shared,
                               DaughtersCuts = dc,
                               CombinationCut = combCut,
                               MotherCut = parentCut,
                               Preambulo = [] )
        
class SLB_DstTag(Hlt2Combiner) : 
    def __init__(self, name, decay, inputs, nickname = None, shared = False) : 
        
        dc = { }
        for child in ["pi+"] :
            dc[child] = ( "(PT > %(Pi_PT_MIN)s)" +
                          "& (P > %(Pi_P_MIN)s)" +
                          "& (TRGHOSTPROB < %(Pi_GHOSTPROB_MAX)s)" )
                
        combCut = ( 'in_range( %(Q_AM_MIN)s, (AM - AM1 - AM2), %(Q_AM_MAX)s )' )
        parentCut = ( "(VFASPF(VCHI2PDOF) < %(Tag_VCHI2PDOF_MAX)s)" +
                      "& in_range( %(Q_M_MIN)s, (M - M1 - M2), %(Q_M_MAX)s )" )

     
        preambulo =[ "BMassHisto = Gaudi.Histo1DDef('b_mass',2000* MeV, 11000 * MeV,200)",
                     "DeltaMassHisto = Gaudi.Histo1DDef('delta_mass',130* MeV, 160 * MeV,100)",
                     "CORRMHisto = Gaudi.Histo1DDef('corrm',2700* MeV, 8600 * MeV,200)"
        ]
        motherMonitor = ("process(monitor(M,BMassHisto,'b_mass'))"
                         ">>process(monitor(CHILD(M-CHILD(M,ABSID=='D0'),ABSID=='D*(2010)+'),DeltaMassHisto,'delta_mass'))"
                         ">>process(monitor(BPVCORRM,CORRMHisto,'corrm'))"
                         ">> ~EMPTY")

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__( self, name, decay, inputs,
                               dependencies = [PV3D('Hlt2')],
                               nickname = nickname,
                               shared = shared,
                               DaughtersCuts = dc,
                               CombinationCut = combCut,
                               MotherCut = parentCut,
                               Preambulo = preambulo,
                               MotherMonitor = motherMonitor)

class SLB_B2D0Mu(Hlt2Combiner)  :
    def __init__(self, name, decay, inputs, nickname = None, shared = False) : 
      
        dc = { }
        for child in ['mu-'] :
            dc[child] = ( "(PT > %(Mu_PT_MIN)s)" +
                          "& (P > %(Mu_P_MIN)s)" +
                          "& (MIPCHI2DV(PRIMARY) > %(Mu_MIPCHI2DV_MIN)s)"+
                          "& (TRGHOSTPROB < %(Mu_GHOSTPROB_MAX)s)" +
                          "& (PIDmu > %(Mu_PIDMU_MIN)s)" )
                        
        combCut = ( "(in_range(%(B_Mass_MIN)s,AM,%(B_Mass_MAX)s))"+
                    "& (ADOCACHI2CUT( %(B_DocaChi2_MAX)s, ''))" )
        
        parentCut = ( "(in_range(%(B_Mass_MIN)s,M,%(B_Mass_MAX)s))" +
                      "&(in_range(%(B_CorrMass_MIN)s,BPVCORRM,%(B_CorrMass_MAX)s))" +
                      "& (VFASPF(VCHI2PDOF) < %(B_VCHI2PDOF_MAX)s)" +
                      "& (BPVDIRA> %(B_DIRA_MIN)s)" +
                      "& ((MINTREE((ABSID=='D0'),VFASPF(VZ))-VFASPF(VZ)) > %(B_D_DZ_MIN)s)") 
        
        preambulo =[ "BMassHisto = Gaudi.Histo1DDef('b_mass',2000* MeV, 11000 * MeV,200)",
                     "DMassHisto = Gaudi.Histo1DDef('d_mass',1770* MeV, 1960 * MeV,100)",
                     "CORRMHisto = Gaudi.Histo1DDef('corrm',2700* MeV, 8600 * MeV,200)"
        ]

        motherMonitor = ("process(monitor(M,BMassHisto,'b_mass'))"
                         ">>process(monitor(CHILD(M,ABSID=='D0'),DMassHisto,'d_mass'))"
                         ">>process(monitor(BPVCORRM,CORRMHisto,'corrm'))"
                         ">> ~EMPTY")

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__( self, name, decay, inputs,
                               dependencies = [PV3D('Hlt2')],
                               tistos = 'TisTosSpec',
                               nickname = nickname,
                               shared = shared,
                               DaughtersCuts = dc,
                               CombinationCut = combCut,
                               MotherCut = parentCut,
                               Preambulo = preambulo,
                               MotherMonitor = motherMonitor)

SLB_D02HH_D0ToKmPip  = SLB_D02HH( 'SLB_D02HH_D0ToKmPip'
                                  , decay = ["[D0 -> K- pi+]cc"]
                                  , inputs = [ Hlt2LoosePions, Hlt2LooseKaons, Hlt2LooseMuons ]
                                  , nickname = 'SLB_D02KmPip'           
                                  , shared = True)

SLB_B2D0Mu_D0ToKmPip  = SLB_B2D0Mu( 'SLB_B2D0Mu_D0ToKmPip'
                                    , decay = ['[B- -> D0 mu-]cc','[B+ -> D0 mu+]cc']
                                    , inputs = [ SLB_D02HH_D0ToKmPip, Hlt2LooseMuons ]
                                    )

SLB_DstToD0pi_D0ToKmPip  = SLB_DstTag( 'SLB_B2D0Mu_D0ToKmPip'
                                       , decay = [ "[D*(2010)+ -> D0 pi+]cc", "[D*(2010)- -> D0 pi-]cc" ]
                                       , inputs = [ SLB_D02HH_D0ToKmPip, Hlt2LoosePions ]
                                       , nickname = 'SLB_SlowPionTag' 
                                       )

SLB_B2DstMu_D0ToKmPip  = SLB_B2D0Mu( 'SLB_B2DstMu_D0ToKmPip'
                                     , decay = [ '[B~0 -> D*(2010)+ mu-]cc', '[B~0 -> D*(2010)+ mu+]cc']
                                     , inputs = [ SLB_DstToD0pi_D0ToKmPip, Hlt2LooseMuons ]
                                     )

SLB_D02HH_D0ToKmKp  = SLB_D02HH( 'SLB_D02HH_D0ToKmKp'
                                 , decay = ["D0 -> K- K+"]
                                 , inputs = [ Hlt2LooseKaons , Hlt2LooseMuons ]
                                 , nickname = 'SLB_D02KmKp'        
                                 , shared = True)

SLB_B2D0Mu_D0ToKmKp  = SLB_B2D0Mu( 'SLB_B2D0Mu_D0ToKmKp'
                                   , decay = ['B- -> D0 mu-','B+ -> D0 mu+']
                                   , inputs = [ SLB_D02HH_D0ToKmKp, Hlt2LooseMuons ]
                                   )

SLB_DstToD0pi_D0ToKmKp  = SLB_DstTag( 'SLB_B2D0Mu_D0ToKmKp'
                                      , decay = [ "D*(2010)+ -> D0 pi+","D*(2010)- -> D0 pi-" ]
                                      , inputs = [ SLB_D02HH_D0ToKmKp, Hlt2LoosePions ]
                                      , nickname = 'SLB_SlowPionTag' 
                                      )

SLB_B2DstMu_D0ToKmKp  = SLB_B2D0Mu( 'SLB_B2DstMu_D0ToKmKp'
                                    , decay = [ '[B~0 -> D*(2010)+ mu-]cc','[B~0 -> D*(2010)+ mu+]cc' ]
                                    , inputs = [ SLB_DstToD0pi_D0ToKmKp, Hlt2LooseMuons ]
                                    )


SLB_D02HH_D0ToPimPip  = SLB_D02HH( 'SLB_D02HH_D0ToPimPip'
                                   , decay = ["D0 -> pi- pi+"]
                                   , inputs = [ Hlt2LoosePions, Hlt2LooseMuons ]
                                   , nickname = 'SLB_D02PimPip'     
                                   , shared = True)

SLB_B2D0Mu_D0ToPimPip  = SLB_B2D0Mu( 'SLB_B2D0Mu_D0ToPimPip'
                                     , decay = ['B- -> D0 mu-','B+ -> D0 mu+']
                                     , inputs = [ SLB_D02HH_D0ToPimPip, Hlt2LooseMuons ]
                                     )

SLB_DstToD0pi_D0ToPimPip  = SLB_DstTag( 'SLB_B2D0Mu_D0ToPimPip'
                                        , decay = ["D*(2010)+ -> D0 pi+","D*(2010)- -> D0 pi-"]
                                        , inputs = [ SLB_D02HH_D0ToPimPip, Hlt2LoosePions ]
                                        , nickname = 'SLB_SlowPionTag' 
                                        )

SLB_B2DstMu_D0ToPimPip  = SLB_B2D0Mu( 'SLB_B2DstMu_D0ToPimPip'
                                      , decay = [ '[B~0 -> D*(2010)+ mu-]cc','[B~0 -> D*(2010)+ mu+]cc' ]
                                      , inputs = [ SLB_DstToD0pi_D0ToPimPip, Hlt2LooseMuons ]
                                    )




    

"""
Module for the selection of semileptonic B decays going to the Turbo stream.
"""
__author__  = "Sascha Stahl sascha.stahl@cern.ch"

from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
_local_m_pip = 139.57018 * MeV
class SLBLines(Hlt2LinesConfigurableUser) :
    __slots__ = {
        'Prescale'  : {},
        'Postscale' : {},
        'Common'    : { 'TisTosSpec'        : [],
                        'Had_GHOSTPROB_MAX' :  1.0,
                        'Had_PT_MIN'        : 200.0*MeV,
                        'Had_P_MIN'         : 2000.0*MeV,
                        'Had_MIPCHI2DV_MIN' : 9.0,
                        'Mu_GHOSTPROB_MAX'  : 1.0,
                        'Mu_PT_MIN'         : 1000.0*MeV,
                        'Mu_P_MIN'          : 3000.0*MeV,
                        'Mu_MIPCHI2DV_MIN'  : 9.0,
                        'Mu_PIDMU_MIN'      : 0.0,
                        'D0_VCHI2PDOF_MAX'  : 25.0,
                        'D0_AMassWin'       : 100.0 *MeV,
                        'D0_MassWin'        : 90.0 *MeV,
                        'D0_BPVVDCHI2_MIN'  : 9.0,
                        'D0_VCHI2PDOF_MAX'  : 9.0,
                        'B_Mass_MIN'        : 2.3*GeV,
                        'B_Mass_MAX'        : 10.0*GeV,
                        'B_CorrMass_MIN'    : 2.8*GeV,
                        'B_CorrMass_MAX'    : 8.5*GeV,
                        'B_DocaChi2_MAX'    : 10.0,
                        'B_DIRA_MIN'        : 0.999,
                        'B_D_DZ_MIN'        : -3.0*mm,
                        'B_VCHI2PDOF_MAX'   : 9.0
                       },
        'SLB_SlowPionTag': { "Pi_PT_MIN"          : 150.0*MeV,
                             "Pi_P_MIN"           : 2000.0*MeV,
                             "Pi_GHOSTPROB_MAX"   : 0.3,
                             "Tag_VCHI2PDOF_MAX"  : 16.0,
                             'Q_AM_MIN'           : 130.0 * MeV - _local_m_pip,
                             'Q_M_MIN'            : 130.0 * MeV - _local_m_pip,
                             'Q_AM_MAX'           : 165.0 * MeV - _local_m_pip,
                             'Q_M_MAX'            : 160.0 * MeV - _local_m_pip
                             },
        'SLB_D02KmPip'   : { "K_PIDK_MIN"  : 5.,
                             "Pi_PIDK_MAX" : 0.
                           },
        'SLB_D02KmKp'    : { "K_PIDK_MIN"  : 5.},
        'SLB_D02PimPip'  : { "Pi_PIDK_MAX" : 0.},
        'PersistReco'    : { 'B2D0Mu_D02KmPipTurbo'    : False,
                             'B2D0Mu_D02KmKpTurbo'     : False,
                             'B2D0Mu_D02PimPipTurbo'   : False,
                            }
        }
    
    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
            if nickname:
                return self._stages[nickname]
            else:
                return self._stages

        
        from Stages import (SLB_B2D0Mu_D0ToKmPip, SLB_B2D0Mu_D0ToKmKp, SLB_B2D0Mu_D0ToPimPip,SLB_D02HH_D0ToKmPip,
                            SLB_B2DstMu_D0ToKmPip,SLB_B2DstMu_D0ToKmKp,SLB_B2DstMu_D0ToPimPip)
        self._stages = {"B2D0Mu_D02KmPipTurbo"   : [SLB_B2D0Mu_D0ToKmPip],
                        "B2D0Mu_D02PimPipTurbo"  : [SLB_B2D0Mu_D0ToPimPip],
                        "B2D0Mu_D02KmKpTurbo"    : [SLB_B2D0Mu_D0ToKmKp],
                        "B2DstMu_D02KmPipTurbo"  : [SLB_B2DstMu_D0ToKmPip],
                        "B2DstMu_D02KmKpTurbo"   : [SLB_B2DstMu_D0ToKmKp],
                        "B2DstMu_D02PimPipTurbo" : [SLB_B2DstMu_D0ToPimPip],
                        }
        
        if nickname:
            return self._stages[nickname]
        else:
            return self._stages
        
    
    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc
        stages = self.stages()
        for (nickname, algos) in self.algorithms(stages):
            linename = 'SLB_' + nickname if nickname != 'SLB_' else nickname
            if "Turbo" in nickname :
                Hlt2Line(linename, prescale = self.prescale,
                         algos = algos, postscale = self.postscale, Turbo = True,
                         PersistReco=self.getProps()['PersistReco'].get(nickname, False))
            else:
                Hlt2Line(linename, prescale = self.prescale,
                         algos = algos, postscale = self.postscale)

###
#  @author Timon Schmelzer <timon.schmelzer@cern.ch>
#  @author Moritz Demmer <moritz.demmer@cern.ch>
#  @date 2017-02-06
#
#  Please contact the above authors before editing this file
#
##

from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from HltTracking.HltPVs import PV3D

from Inputs import KsLLTF, KsDD

class Hb2V0V0_Combiner(Hlt2Combiner):
  def __init__(self, name, inputs, decay, is_ld=False):
    deps = [PV3D('Hlt2')]
    
    # Combination of long and downstream tracks
    ld_extension = ""
    if is_ld:
        ld_extension = " & (AHASCHILD(INTREE((ABSID=='pi+') & (ISLONG))))" + \
                       " & (AHASCHILD(INTREE((ABSID=='pi+') & (ISDOWN)))) "

    combi_cuts = (   '( AM > %(Hb_MASS_MIN)s )' +
                  ' & ( AM < %(Hb_MASS_MAX)s )' +
                  " & ( ACUTDOCA(%(Hb_DOCA)s, '') )" +
                  ld_extension
                  )
    mother_cut = (   '( BPVDIRA > %(Hb_DIRA)s )' +
                  ' & ( VFASPF(VCHI2) < %(Hb_VTXCHI2)s )'
                  )
    Hlt2Combiner.__init__(self,
                          name,
                          decay,
                          inputs,
                          dependencies=deps,
                          CombinationCut=combi_cuts,
                          MotherCut=mother_cut)

class KS_Filter(Hlt2ParticleFilter):
  """Filter KS depending on track type."""
  def __init__(self, name, inputs, track_type):
    possible_track_types = ['DD', 'LL']
    if track_type not in possible_track_types:
        raise KeyError('Track type not found. Choose one of these: {0}'.format(possible_track_types))
    if track_type == 'DD':
      cut =  ( " ( ADMASS('KS0') < %(KS_DD_MASS_WINDOW)s )" +
             ' & ( BPVVDCHI2 > %(KS_DD_BPVVDCHI2)s )' +
             ' & ( CHILDCUT( ( TRCHI2DOF < %(KS_DD_TRCHI2DOF)s ), 1) )' +
             ' & ( CHILDCUT( ( TRCHI2DOF < %(KS_DD_TRCHI2DOF)s ), 2) )'
             )
    elif track_type == 'LL':
      cut = ( " ( ADMASS('KS0') < %(KS_LL_MASS_WINDOW)s )" +
             ' & ( BPVVDCHI2 > %(KS_LL_BPVVDCHI2)s )' +
             ' & ( CHILDCUT( ( TRCHI2DOF < %(KS_LL_TRCHI2DOF)s ), 1) )' +
             ' & ( CHILDCUT( ( TRCHI2DOF < %(KS_LL_TRCHI2DOF)s ), 2) )' +
             ' & ( CHILDCUT( ( TRGHOSTPROB < %(KS_LL_TRGHOSTPROB)s ), 1) )' +
             ' & ( CHILDCUT( ( TRGHOSTPROB < %(KS_LL_TRGHOSTPROB)s ), 2) )'
             )
    Hlt2ParticleFilter.__init__(self,
                                name,
                                cut,
                                inputs,
                                UseP2PVRelations = False)

# Filter LL/DD KS
KS_DD_Filtered = KS_Filter('KS_DD', [KsDD], 'DD')
KS_LL_Filtered = KS_Filter('KS_LL', [KsLLTF], 'LL')


# 2 body decays
Hb2KSKSDD = Hb2V0V0_Combiner('Hb2V0V0_DD', [KS_DD_Filtered], 'B_s0 -> KS0 KS0')
Hb2KSKSLD = Hb2V0V0_Combiner('Hb2V0V0_LD', [KS_DD_Filtered, KS_LL_Filtered],
                             'B_s0 -> KS0 KS0', is_ld=True)
Hb2KSKSLL = Hb2V0V0_Combiner('Hb2V0V0_LL', [KS_LL_Filtered], 'B_s0 -> KS0 KS0')


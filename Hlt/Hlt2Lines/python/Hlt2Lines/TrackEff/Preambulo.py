__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

def D0Preambulo():
    return  [
        "from numpy import inner, cross",
        "InnerProduct = ((CHILD(PX,1)*CHILD(PX,2)+CHILD(PY,1)*CHILD(PY,2)+CHILD(PZ,1)*CHILD(PZ,2))/CHILD(P,2))",
        "MassConstraintMomentum = 0.5*((1864.8**2 - CHILD(M,1)**2 - CHILD(M,2)**2)/(CHILD(E,1)*math.sqrt(1+(CHILD(M,2)/20000.)**2)-InnerProduct))",
        "MassConstraintMomentum = 0.5*((1864.8**2 - CHILD(M,1)**2 - CHILD(M,2)**2)/(CHILD(E,1)*math.sqrt(1+(CHILD(M,2)/MassConstraintMomentum)**2)-InnerProduct))",
        "mass_constraint_d0_momentum_vector = [ CHILD(PX,1) + MassConstraintMomentum * CHILD(PX,2)/CHILD(P,2), CHILD(PY,1) + MassConstraintMomentum * CHILD(PY,2)/CHILD(P,2), CHILD(PZ,1)+ MassConstraintMomentum * CHILD(PZ,2)/CHILD(P,2)]",
        "mass_constraint_d0_momentum = math.sqrt( mass_constraint_d0_momentum_vector[0]**2+mass_constraint_d0_momentum_vector[1]**2+mass_constraint_d0_momentum_vector[2]**2)",
        "normalised_mass_constraint_d0_momentum_vector = [mass_constraint_d0_momentum_vector[i]/mass_constraint_d0_momentum for i in range (0,3)]",
        
        "D0_ENDVERTEX_POSITION = [VFASPF(VX), VFASPF(VY), VFASPF(VZ)]",
        "D0_PV_POSITION = [BPV(VX),BPV(VY),BPV(VZ)]",
        
        "LambdaFactor = - (inner([VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], normalised_mass_constraint_d0_momentum_vector))",
        "D0_MASS_CONSTRAINT_IP_VECTOR = [(D0_ENDVERTEX_POSITION[i] + LambdaFactor * normalised_mass_constraint_d0_momentum_vector[i]) - D0_PV_POSITION[i] for i in range (0,3)]",
        "D0_MASS_CONSTRAINT_IP = math.sqrt(D0_MASS_CONSTRAINT_IP_VECTOR[0]**2+D0_MASS_CONSTRAINT_IP_VECTOR[1]**2+D0_MASS_CONSTRAINT_IP_VECTOR[2]**2)",
        
        "perp_vector = cross( [CHILD(PX,1), CHILD(PY,1), CHILD(PZ,1) ], [ CHILD(PX,2), CHILD(PY,2), CHILD(PZ,2) ] )",
        "perp_vector_norm = math.sqrt(perp_vector[0]**2+perp_vector[1]**2+perp_vector[2]**2)",
        "D0_IP_PERP = abs(inner( [VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], perp_vector )/perp_vector_norm)"
        ]

def DstPreambulo():
    return  [
        'D0_M  = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2) )',
        'Dst_M = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2), LoKi.Child.Selector(2) )']

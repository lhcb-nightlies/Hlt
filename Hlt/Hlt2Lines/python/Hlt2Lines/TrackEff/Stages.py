__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] :
            if i not in modules : modules.append(i)
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, "TrackEff" + name, code, [tracks], shared = True, nickname = 'TrackGEC')

class TrackEffTagFilter(Hlt2ParticleFilter):
    def __init__(self, name, pid, inputs):
        if pid == "K":
            pid_cut = "(PIDK > %(TagK_MinPIDK)s  )"
        elif pid == "Pi":
            pid_cut = "(PIDK < %(TagPi_MaxPIDK)s  )"

        cut = "(PT > %(Tag_MinPT)s) & (P>%(Tag_MinP)s) & " + pid_cut + " & (MIPCHI2DV(PRIMARY) > %(Tag_MinIPCHI2)s) & (TRCHI2DOF <%(Tag_MaxTrChi2)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs)

class D0ToKpiCombiner(Hlt2Combiner):
    def __init__(self, name,inputs):
        combCut   = (  "(in_range( %(D0_MinAM)s, AM, %(D0_MaxAM)s ))"
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)"
                     + "& (ACUTDOCACHI2(%(D0_MaxDOCACHI2)s, ''))"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(D0_AMAXIPCHI2)s) "
                     + "& (AOVERLAP(1,2, LHCb.LHCbID.Velo) < %(D0_MaxOverlap)s)")

        motherCut = (  "(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s)"
                     + "& (math.log(BPVVD)< %(D0_MaxLogVD)s) "
                     + "& (VFASPF(VCHI2/VDOF)< %(D0_MaxVCHI2NDF)s)"
                     + "& (abs(CHILD(1, ETA) - CHILD(2, ETA)) < %(D0_MaxDeltaEta)s)"
                     + "& (math.log(D0_MASS_CONSTRAINT_IP) < %(D0_MCONST_MaxLogIPCHI2)s)"
                     + "& (math.log(D0_IP_PERP) < %(D0_MaxLogIPPerp)s)"
                     + "& (in_range( %(D0_MinSimpleFitM)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(D0_MaxSimpleFitM)s ))")

        from HltTracking.HltPVs import PV3D
        from Preambulo import D0Preambulo
        Hlt2Combiner.__init__(self, name,["[D0 -> K- pi+]cc",
                                          "[D0 -> K- pi-]cc"], inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos = 'TisTosSpec',
                              CombinationCut = combCut, MotherCut = motherCut,
                              Preambulo = D0Preambulo())

# Filters for the 4-body mode
class FourBodyTrackEffTagFilter(Hlt2ParticleFilter):
    def __init__(self, name, pid, inputs):
        if pid == "K":
            pid_cut = "(PIDK > %(TagK_MinPIDK)s  )"
        elif pid == "Pi":
            pid_cut = "(PIDK < %(TagPi_MaxPIDK)s  )"
            
        cut = "(PT > %(Tag_4Body_MinPT)s) & (P>%(Tag_4Body_MinP)s) & " + pid_cut + " & (MIPCHI2DV(PRIMARY) > %(Tag_4Body_MinIPCHI2)s) & (TRCHI2DOF <%(Tag_4Body_MaxTrchi2)s)"

        Hlt2ParticleFilter.__init__(self, name, cut, inputs)


class K2piLongTracksCombiner(Hlt2Combiner):
    def __init__(self, name,inputs):
        combCut   = (" (ANUM(PT > %(Kst_MinAPTTwoDaughters)s)>2) "
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(Kst_MAXCHILDMinIPCHI2)s) "
                     + "& (APT > %(Kst_MinAPT)s)"
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)"
                     + "& (in_range( %(Kst_MinAM)s,AM, %(Kst_MaxAM)s ))")
        
        motherCut = (  "(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s)"  
                     + "& (VFASPF(VCHI2/VDOF) < %(Kst_MaxVCHI2NDF)s)"
                     + "& (MIPCHI2DV(PRIMARY) > %(Kst_MinIPCHI2)s)")
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,['[K*(892)+ -> K- pi+ pi+]cc',
                                          '[K*(892)+ -> K- pi+ pi-]cc',
                                          '[K*(892)+ -> K- pi- pi-]cc'], 
                              inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos = 'TisTosSpec',
                              CombinationCut = combCut, 
                              MotherCut = motherCut)

class D0ToK3piCombiner(Hlt2Combiner):
    def __init__(self, name,inputs):
        combCut   = (  "(in_range( %(D0_MinAM)s,AM, %(D0_MaxAM)s ))"
                     + "& (AMAXCHILD(MIPCHI2DV(PRIMARY)) > %(D0_AMAXIPCHI2)s) "
                     + "& (AMAXDOCA('') < %(D0_MaxDOCA)s)")
        motherCut = (  "(BPVVDZ > %(D0_MinVDZ)s) & (BPVVDCHI2 > %(D0_MinVDCHI2)s)"
                     + "& (VFASPF(VCHI2/VDOF)< %(D0_MaxVCHI2NDF)s)" 
                     + "& (abs(CHILD(1, ETA) - CHILD(2, ETA)) < %(D0_MaxDeltaEta)s)"
                     + "& (math.log(D0_MASS_CONSTRAINT_IP) < %(D0_MCONST_MaxLogIPCHI2)s)"
                     + "& (math.log(D0_IP_PERP) < %(D0_MaxLogIPPerp)s)"
                     + "& (in_range( %(D0_MinSimpleFitM)s, POINTINGMASS ( LoKi.Child.Selector(0), LoKi.Child.Selector(1), LoKi.Child.Selector(2) ), %(D0_MaxSimpleFitM)s ))")

        from HltTracking.HltPVs import PV3D
        from Preambulo import D0Preambulo
        
        # The K*+ always has a K- daughter, which tags the D0 decay. 
        Hlt2Combiner.__init__(self, name, ["[D0 -> K*(892)+ pi+]cc", 
                                           "[D0 -> K*(892)+ pi-]cc"] , inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos = 'TisTosSpec',
                              CombinationCut = combCut, MotherCut = motherCut,
                              Preambulo = D0Preambulo())

class DstTagger(Hlt2Combiner):
    def __init__(self, name,inputs):
        DstCombinationCut = "(APT > %(Dst_MinAPT)s ) & (AALLSAMEBPV(-1, -1, -1))"
        #DstMotherCut      = "((Dst_M - D0_M) < %(Dst_MaxSimpleFitDeltaMass)s)"
        DstMotherCut = "ALL"
        DaughtersCuts     = {'pi+' : "(PT > %(Slowpi_MinPt)s)"}
        
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, ["[D*(2010)+ -> D0 pi+]cc"], inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              CombinationCut = DstCombinationCut, 
                              MotherCut=DstMotherCut,
                              DaughtersCuts = DaughtersCuts,  )

class DstCloneKiller(Hlt2ParticleFilter):
    def __init__(self, name, inputs):
        cut = "(MAXOVERLAP( (ABSID == 'pi+') | (ABSID=='K+') ) < %(D0_MaxOverlap)s)"
        
        Hlt2ParticleFilter.__init__(self, name, cut, inputs                                  )

class DstPVDeltaMassFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs):
        """ The monitoring of the pointing delta-mass """
        from HltLine.Hlt2Monitoring import Hlt2MonitorMinMax
        from Preambulo import DstPreambulo
        
        cut = "((Dst_M - D0_M) < %(Dst_MaxSimpleFitDeltaMass)s)"
        args = {'PreMonitor'  : (Hlt2MonitorMinMax("(Dst_M - D0_M)","D^{*} M_{PV Constraint} - D^{0} M_{PV Constraint} [MeV]", 100, 225,"Pointing_DeltaMass_in",nbins=50)),
                'PostMonitor'  :(Hlt2MonitorMinMax("(Dst_M - D0_M)","D^{*} M_{PV Constraint} - D^{0} M_{PV Constraint} [MeV]", 100, 225, "Pointing_DeltaMass_out",nbins=50))
                }
        
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    Preambulo = DstPreambulo(),
                                    **args
                                    )

class DstDTFFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs):
        from HltLine.Hlt2Monitoring import Hlt2MonitorMinMax
        cut = "(DTF_FUN ( M , True ,  'D0' , %(Dst_MaxDTFCHI2NDF)s , %(Dst_MaxDTFM)s+1.0 ) < %(Dst_MaxDTFM)s)"
        args = {'PreMonitor'  : (Hlt2MonitorMinMax("DTF_FUN(M,True,'D0')","D^{*} M_{DTF}(D^0,PV) [MeV]", 2000,2060,"Dst_DTF_M_in",nbins=50)
                                 +"&"+Hlt2MonitorMinMax("DTF_CHI2NDOF(True,'D0')","DecayTreeFitter D^{*} #chi^{2}/ndf", 0,20,"Dst_DTF_CHI2_in",nbins=50)),
                'PostMonitor'  : (Hlt2MonitorMinMax("DTF_FUN(M,True,'D0')","DecayTreeFitter D^{*} mass [MeV]", 2000,2060,"Dst_DTF_M_out",nbins=50)
                                  +"&"+Hlt2MonitorMinMax("DTF_CHI2NDOF(True,'D0')","DecayTreeFitter D^{*} #chi^{2}/ndf", 0,20,"Dst_DTF_CHI2_out",nbins=50))
                }
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,**args)

from Inputs import GoodPions, GoodKaons, VeloKaons, VeloPions , SlowPions

#### pion probe
TagKaons   = TrackEffTagFilter('D2KPi_TagK', pid='K', inputs = [GoodKaons])
TagPions   = TrackEffTagFilter('D2KPi_TagPi', pid='Pi', inputs = [GoodPions])

D0ToKpiPionProbe  = D0ToKpiCombiner('D2KPi_D0ForPionProbe', inputs = [TagKaons,VeloPions])
DstD0ToKpiPionProbe  = DstTagger('Dst',inputs=[D0ToKpiPionProbe,SlowPions])
CloneKilledDstD0KPiPionProbe = DstCloneKiller('DstNoClones', inputs=[DstD0ToKpiPionProbe])
PreFilteredDstD0ToKpiPionProbe=DstPVDeltaMassFilter('DstDMPVConForPionProbe', [CloneKilledDstD0KPiPionProbe])
FilteredDstD0ToKpiPionProbe  = DstDTFFilter('DstDTFForPionProbe',inputs=[PreFilteredDstD0ToKpiPionProbe])

##### kaon probe
D0ToKpiKaonProbe   = D0ToKpiPionProbe.clone('D2KPi_D0ForKaonProbe', decay = ["[D0 -> pi+ K-]cc","[D0 -> pi+ K+]cc"],
                                            inputs = [TagPions,VeloKaons])
DstD0ToKpiKaonProbe  = DstTagger('Dst',inputs=[D0ToKpiKaonProbe,SlowPions])
CloneKilledDstD0KPiKaonProbe = DstCloneKiller('DstNoClones', inputs=[DstD0ToKpiKaonProbe])
PreFilteredDstD0ToKpiKaonProbe=DstPVDeltaMassFilter('DstDMPVConForKaonProbe', [CloneKilledDstD0KPiKaonProbe])
FilteredDstD0ToKpiKaonProbe  = DstDTFFilter('DstDTFForKaonProbe',inputs=[PreFilteredDstD0ToKpiKaonProbe])

#### pion probe, 4-body
TagKaonsForFourBody   = TrackEffTagFilter('D2K3Pi_TagK', pid='K', inputs = [GoodKaons])
TagPionsForFourBody   = TrackEffTagFilter('D2K3Pi_TagPi', pid='Pi', inputs = [GoodPions])

ThreeLongTrackCombinations  = K2piLongTracksCombiner('D2K3Pi_KStar', inputs = [TagPionsForFourBody,TagKaonsForFourBody])
D0ToK3piPionProbe = D0ToK3piCombiner('D2K3Pi_D0ForFourBody', inputs=[ ThreeLongTrackCombinations, VeloPions])
DstD0ToK3piPionProbe  = DstTagger('D2K3Pi_Dst',inputs=[D0ToK3piPionProbe,SlowPions])
CloneKilledDstFourBodyPionProbe = DstCloneKiller('D2K3Pi_DstCloneKiller', inputs=[DstD0ToK3piPionProbe])
FilteredDstD0ToK3piPionProbe= DstPVDeltaMassFilter('D2K3Pi_DstPVFilter', inputs=[CloneKilledDstFourBodyPionProbe])
#FilteredDstD0ToK3piPionProbe  = DstDTFFilter('D2K3Pi_DstDTF',inputs=[PreFilteredDstD0ToK3piPionProbe])

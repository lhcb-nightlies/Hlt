__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class TrackEffLines(Hlt2LinesConfigurableUser):
    __slots__ = {'Prescale' : {},
                 'TrackGEC' : {'NTRACK_MAX'           : 180},
                 'Common' : {'TisTosSpec'               : "Hlt1Track.*Decision%TOS"
                             , 'D0_MinAM' : 900.*MeV
                             , 'D0_MaxAM' : 2250.*MeV
                             
                             , 'D0_MinVDZ' : 3.5*mm
                             , 'D0_MinVDCHI2' : 25
                             
                             , 'D0_MaxLogVD': 3.5
                             
                             , 'D0_MaxOverlap': 0.9
                             , 'D0_MaxVCHI2NDF' : 3.5
                             , 'D0_MaxDeltaEta' : 1.25
                             
                             , 'D0_MaxDOCA' : 0.1 * mm
                             , 'D0_MaxDOCACHI2' : 5
                             
                             , 'D0_MinSimpleFitM' : 1865.-450. * MeV
                             , 'D0_MaxSimpleFitM' : 1865.+650. * MeV # the fit more often overshoots the momentum...
                             
                             , 'Dst_MinAPT'  : 1750. *MeV
                             , 'Dst_MaxDTFM' : 2030. *MeV
                             , 'Dst_MaxDTFCHI2NDF' : 3.0
                             
                             , 'Dst_MaxSimpleFitDeltaMass' : 225. * MeV
                             
                             , 'TagK_MinPIDK' : -5.
                             , 'TagPi_MaxPIDK' : 10.
                             
                             , 'Slowpi_MinPt' : 110*MeV
                             
                             , 'D0_MCONST_MaxLogIPCHI2' : -2.5
                             , 'D0_MaxLogIPPerp' : -3
                             },
                 'D2KPi_TagPi': {'Tag_MinPT': 1300 * MeV
                                , 'Tag_MinP': 5 * GeV
                                , 'Tag_MinIPCHI2': 8
                                , 'Tag_MaxTrChi2': 3.5},
                 'D2KPi_TagK':{
                              'Tag_MinPT': 1300 * MeV
                             , 'Tag_MinP': 5 * GeV
                             , 'Tag_MinIPCHI2': 8
                             , 'Tag_MaxTrChi2': 3.5},
                 'D2K3Pi_TagK':{            
                               'Tag_MinPT' : 250. * MeV
                             , 'Tag_MinP' : 2. * GeV
                             , 'Tag_MinIPCHI2' : 4.
                             , 'Tag_MaxTrChi2' : 3.5},
                 'D2K3Pi_TagPi':{            
                               'Tag_MinPT' : 250. * MeV
                             , 'Tag_MinP' : 2. * GeV
                             , 'Tag_MinIPCHI2' : 4.
                             , 'Tag_MaxTrChi2' : 3.5},
                 'D2K3Pi_KStar': {'Kst_MinAPTTwoDaughters': 400 * MeV,
                                  'Kst_MinAPT': 2500. * MeV,
                                  'Kst_MaxVCHI2NDF': 4,
                                  'Kst_MAXCHILDMinIPCHI2': 5,
                                  'Kst_MinIPCHI2': 6,
                                  'Kst_MinAM': 900. * MeV,
                                  'Kst_MaxAM': 1800. * MeV
                            },
                 'D2K3Pi_D0ForFourBody': {
                               'D0_AMAXIPCHI2' : 7
                             , 'D0_MCONST_MaxLogIPCHI2' : -2.5
                             , 'D0_MaxLogIPPerp' : -3
                             , 'D0_MinVDZ' : 3.5*mm
                             },
                 'D2KPi_D0ForPionProbe': {
                               'D0_AMAXIPCHI2' : 12
                             , 'D0_MCONST_MaxLogIPCHI2' : -2.5
                             , 'D0_MaxLogIPPerp' : -3.0
                             , 'D0_MinVDCHI2' : 30
                             },
                 'D2KPi_D0ForKaonProbe': {
                               'D0_AMAXIPCHI2' : 12
                             , 'D0_MCONST_MaxLogIPCHI2' : -3.0
                             , 'D0_MaxLogIPPerp' : -3.0
                             , 'D0_MinVDCHI2' : 30
                             }}  
    def __apply_configuration__(self):
        from HltLine.HltLine import Hlt2Line
        stages = self.stages();
        
        for (nickname, algos) in self.algorithms(stages):
            Hlt2Line('TrackEff_' + nickname+'Turbo', 
                     prescale = self.prescale,
                     algos = algos, 
                     postscale = self.postscale,
                     Turbo=True,
                     PersistReco=True)

    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
          if nickname:
            return self._stages[nickname]
          else:
            return self._stages
        
        from Stages import FilteredDstD0ToKpiPionProbe
        from Stages import FilteredDstD0ToKpiKaonProbe
        from Stages import FilteredDstD0ToK3piPionProbe
        
        self._stages = {'D0ToKpiPionProbe'  : [FilteredDstD0ToKpiPionProbe],
                  'D0ToKpiKaonProbe'  : [FilteredDstD0ToKpiKaonProbe],
                  'D0ToK3piPionProbe' : [FilteredDstD0ToK3piPionProbe]}

    
        if nickname:
          return self._stages[nickname]
        else:
          return self._stages

            

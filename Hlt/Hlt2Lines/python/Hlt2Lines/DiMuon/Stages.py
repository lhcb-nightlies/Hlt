# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2ExtraCombiner import Hlt2ExtraCombiner

from HltTracking.HltPVs import PV3D
from Inputs import *

class DiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MM > %(MinMass)s) & (PT> %(Pt)s)" +
                " & (MINTREE('mu-' == ABSID, PT) > %(MuPt)s)" +
                " & (VFASPF(VCHI2PDOF)<%(VertexChi2)s )" +
                " & (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s)")
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True)

class DetachedDiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > %(IPChi2)s)" +
                " & (BPVDLS > %(DLS)s)")
        inputs = [DiMuonFilter('DiMuon')]
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 10000, 10000, 'M_in',  nbins = 100),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 10000, 10000, 'M_out', nbins = 100)}
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    dependencies = [PV3D('Hlt2')],
                                    UseP2PVRelations = False, shared = True, **args)

class DetachedDiMuonHeavyFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MM > %(MinMass)s) & (PT> %(Pt)s)" +
                " & (MINTREE('mu-' == ABSID, PT) > %(MuPt)s)" +
                " & (VFASPF(VCHI2PDOF)<%(VertexChi2)s )" +
                " & (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s)" +
                " & (MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > %(IPChi2)s)" +
                " & (BPVDLS > %(DLS)s)")
        inputs = [TrackFittedDiMuon]
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 4000, 1000, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 4000, 1000, 'M_out', nbins = 25)}

        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    dependencies = [PV3D('Hlt2')],
                                    UseP2PVRelations = False, shared = True, **args)


class SoftDiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MINTREE('mu-' == ABSID, MIPDV(PRIMARY)) > %(IP)s)" +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),1))" +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),2))" +
                " & (MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > %(IPChi2Min)s)" +
                " & (MAXTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) < %(IPChi2Max)s)" +
                " &  (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu)s)" +
                " & ( VFASPF (sqrt(VX*VX+VY*VY)) > %(Rho)s) " +
                " & ( VFASPF ( VZ ) < %(SVZ)s) " +
                " & ((MIPDV(PRIMARY)/BPVVDZ)< %(MaxIpDistRatio)s)"+
                " & (MM < %(MaxMass)s)" +
                " & (VFASPF(VCHI2PDOF)<%(VertexChi2)s )" +
                " & (DOCAMAX < %(doca)s)" +
                " & (BPVVDZ > %(MinVDZ)s) " +
                "&  (BPVDIRA > %(MinBPVDira)s) " +
                "& (PCUTA (ALV (1,2) < %(cosAngle)s))"
                )
        inputs = [TrackFittedDiMuon]
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 500, 500, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 500, 500, 'M_out', nbins = 25)}
        
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True,
                                    dependencies = [PV3D('Hlt2')],
                                    UseP2PVRelations = False, **args)


class BaseDiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MAXTREE('mu-' == ABSID,TRCHI2DOF) < %(TrChi2Tight)s )" +
                "& (MINTREE('mu-' == ABSID,PT) > %(MuPt)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s )"  +
                "& (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),1))" +
                "& (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),2))" +
                "& ( %(PIDCut)s ) ")    
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True)

class JpsiFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS('J/psi(1S)') < %(MassWindow)s) " +
                "& (PT > %(Pt)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_out', nbins = 25)
                                + " & " + Hlt2MonitorMinMax("PT","PT(#mu#mu)", 0, 10000, 'JPsiPT_out', nbins = 100)
                                + " & " + Hlt2MonitorMinMax("MINTREE('mu-' == ABSID, PT)", "MINTREE(mu-==ABSID, PT)", 0, 10000, 'MuPT_out', nbins = 100)
                                + " & " + Hlt2MonitorMinMax("VFASPF(VCHI2PDOF)", "VFASPF(VCHI2PDOF)", 0, 25, 'JPsiVeterxChi2_out', nbins = 100)}
        inputs = [BaseDiMuonFilter('BaseDiMuon')]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True, **args)
                                        
class Psi2SFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS(3686.09) < %(MassWindow)s) "
                "& (PT > %(Pt)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3686, 200, 'M_out', nbins = 25)}
        inputs = [BaseDiMuonFilter('BaseDiMuon')]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True, **args)


class Psi2SLowPtFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS(3686.09) < %(MassWindow)s) " +
                "& (PT < %(PtMax)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3686, 200, 'M_out', nbins = 25)}
        inputs = [BaseDiMuonFilter('BaseDiMuon')]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True, **args)


class DetachedCommonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(PT > %(Pt)s) " +
                "& (MAXTREE('mu-' == ABSID,TRCHI2DOF) < %(TrChi2Tight)s )" +
                "& (MINTREE('mu-' == ABSID,PT) > %(MuPt)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s )"  +
                "& (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),1))" +
                "& (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),2)) "+
                "& (BPVDLS > %(DLS)s)")
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True,
                                    dependencies = [PV3D('Hlt2')],
                                    UseP2PVRelations = False)

class DetachedJpsiFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS('J/psi(1S)') < %(MassWindow)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'UseP2PVRelations' : False,
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_out', nbins = 25)}
        inputs = [DetachedCommonFilter('DetachedCommon')]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    dependencies = [PV3D('Hlt2')],
                                    **args)


class DetachedPsi2SFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS(3686.09) < %(MassWindow)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'UseP2PVRelations' : False,
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 3686, 200, 'M_out', nbins = 25)}
        inputs = [DetachedCommonFilter('DetachedCommon')]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    dependencies = [PV3D('Hlt2')],**args)

class BFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(M > %(MinMass)s) " +
                " & (M < %(MaxMass)s)" +
                " & (DOCAMAX < %(doca)s)" +
                " & (MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > %(IPChi2Min)s)" +
                "& (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s) ")

        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 5300, 700, 'M_out', nbins=25)}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    dependencies = [PV3D('Hlt2')], **args)

class BFilterUB(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(M > %(MinMass)s) " +
                " & (M < %(MaxMass)s)" +
                " & (DOCAMAX < %(doca)s)" +
                " & (BPVLTIME() > %(MinBmumuLifetime)s)" +
                "& (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s) ")
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 5300, 700, 'M_out', nbins=25)}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    dependencies = [PV3D('Hlt2')],
                                    **args)

class BFilterPrompt(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(M > %(MinMass)s) " +
                " & (M < %(MaxMass)s)" +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),1))" +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),2))" +
                " & (DOCAMAX < %(doca)s)" +
                " & (MAXTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) < %(IPChi2MaxPrompt)s)" +
                "& (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s) "+
                "& ( %(PIDCut)s ) "
                )

        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 5300, 700, 'M_out', nbins=25)}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                     dependencies = [PV3D('Hlt2')],
                                    **args)


class UpsilonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(M > %(MinMass)s) " +
                "& (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TrChi2Tight)s) " +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s) " +
                "& ( %(PIDCut)s ) ")
        
        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)", 3097, 200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 9460, 700, 'M_out', nbins = 25)}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, **args)
                                        
class ZFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(MM>%(MinMass)s) & (PT>%(Pt)s)")

        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PreMonitor'  : Hlt2Monitor("M", "M(#mu#mu)",  3097,   200, 'M_in',  nbins = 25),
                'PostMonitor' : Hlt2Monitor("M", "M(#mu#mu)", 90000, 10000, 'M_out', nbins=25)}
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, **args)

class ChicJpsiDiMuonCombiner(Hlt2Combiner):
    def __init__(self, name, shared = True):

        #decay
        decay = "chi_c1(1P) -> J/psi(1S) mu+ mu-"
        
        #mass window around (chic_1 + chic_2)/2.
        cc = ("(ADAMASS(3530.0) < %(MassWindow_comb)s)")
        
        #I want the Jpsi and bachelor muons to come from the same vertex
        mc = "(VFASPF(VCHI2PDOF) < %(VertexChi2)s)"
        
        inputs = [JpsiFilter('JPsiNoPT'), BiKalmanFittedMuons]
        
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              MotherCut = mc,
                              CombinationCut = cc,
                              shared = shared,
                              Preambulo = ["massHisto = Gaudi.Histo1DDef('chic_mass', 3450 * MeV, 3600 * MeV, 150)"],
                              MotherMonitor = "process(monitor(M, massHisto, 'chic_mass')) >> ~EMPTY",
                              )

#instance of the ChicJpsiDiMuonCombiner for the ChicXTurbo line
ChicJpsiDiMuon = ChicJpsiDiMuonCombiner('ChicXTurbo')

class ChicXExtraCombiner(Hlt2ExtraCombiner):
    def __init__(self, name, decay, inputs, shared = True):
        
        #particles to save
        part_save = "[X -> chi_c1(1P) ^X]CC"
        
        #I want the chic and the bachelor X to come from the same vertex
        mc = "(VFASPF(VCHI2PDOF) < %(VertexChi2)s)"
        
        Hlt2ExtraCombiner.__init__(self, name, decay, inputs,
                                   MotherCut = mc,
                                   CombinationCut = "AALL",
                                   shared = shared,
                                   ParticlesToSave = part_save
                                   )
        
        

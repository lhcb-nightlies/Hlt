#
# Do not edit without permission of the HLT Muon responsible.
#
""" Module for the selection of inclusive DiMuons. This is the base of a wide range of analyses in various WG.
These are managed by the HLT Muon responsible. 
"""
__version__ = "$Revision: $"
__author__  = "Francesco Dettori francesco.dettori@cern.ch"

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
class DiMuonLines(Hlt2LinesConfigurableUser) :
    __slots__ = {'_stages' : {},
                 'Prescale' : {},
                 'PersistReco' : {'JPsiTurbo'                          : True,
                                  'Psi2SLowPTTurbo'                    : True,
                                  'Psi2STurbo'                         : True,
                                  'UpsilonTurbo'                       : True,
                                  },

                 'Common' :        {'TrChi2'     :   10,
                                    'TrChi2Tight':    5},

                 'DiMuon' :        {'MinMass'    :     0 * MeV,
                                    'Pt'         :   600 * MeV,
                                    'MuPt'       :   300 * MeV,
                                    'VertexChi2' :    9},
                 
                 'BaseDiMuon' :  {  'MuPt'       :     0 * MeV,
                                    'VertexChi2' :    25,
                                    'TRACK_TRGHOSTPROB_MAX': 0.4,
                                    'PIDCut'     : "(MINTREE('mu-' == ABSID, PROBNNmu)>0.1)"},

                
                 'JPsi' :          {'MassWindow' :   120 * MeV,
                                    'Pt'         :     0 * MeV},
                 
                 'JPsiNoPT' :      {'MassWindow' :    80 * MeV,
                                    'Pt'         :     0 * MeV},
                 
                 'JPsiHighPT' :    {'Pt'         :  2000 * MeV,
                                    'MassWindow' :   120 * MeV},

                 'Psi2S' :         {'MassWindow' :   120 * MeV,
                                    'Pt'         :  0 * MeV},

                 'Psi2SLowPT' :    {'MassWindow' :   120 * MeV,
                                    'PtMax'      :  2500 * MeV},

                 'Psi2SHighPT' :   {'MassWindow' :   120 * MeV,
                                    'Pt'         :  2000 * MeV},

                 'B' :             {'MinMass'    :   4700 * MeV,
                                    'MaxMass'    :   6500 * MeV,
                                    'doca'       :   0.9 * mm,
                                    'IPChi2Min'  :   2,
                                    'VertexChi2' :    9},
                 'BUB' :             {'MinMass'    :   4700 * MeV,
                                    'MaxMass'    :   7000 * MeV,
                                    'doca'       :   0.9 * mm,
                                    'MinBmumuLifetime'  :   0.1 * picosecond,
                                    'VertexChi2' :    9},
                 'BPrompt' :       {'MinMass'    :   5000 * MeV,
                                    'MaxMass'    :   7000 * MeV,
                                    'doca'       :   0.1 * mm,
                                    'IPChi2MaxPrompt'  :  1,
                                    'TRACK_TRGHOSTPROB_MAX': 0.2,
                                    'VertexChi2' :    1,
                                    'PIDCut'     : "(MINTREE('mu-' == ABSID, PROBNNmu)>0.3)"},

                 'Z' :             {'MinMass'    : 40000 * MeV,
                                    'Pt'         :     0 * MeV},


                 'Soft' :          {'IP'         :   0.3 * mm ,
                                    'IPChi2Min'  :   9,
                                    'IPChi2Max'  :   10000000000000,
                                    'TTHits'     :      -1,
                                    'TRACK_TRGHOSTPROB_MAX': 0.4,
                                    'MaxMass'    :   1000 * MeV,
                                    'MuProbNNmu' :    0.05,
                                    'VertexChi2' :    25,
                                    'Rho'        :     3,
                                    'SVZ'        :   650,
                                    'doca'       :   0.3,
                                    'MinVDZ'     :     0,
                                    'MinBPVDira' :     0,
                                    'MaxIpDistRatio':  1./60,
                                    'cosAngle'   :0.999998
                                    },

                 'DetachedCommon' :  {'Pt'         :     0 * MeV,
                                      'MuPt'       :     0 * MeV,
                                      'VertexChi2' :    25,
                                      'TRACK_TRGHOSTPROB_MAX': 0.4,
                                      'DLS'        :     3},
                 
                 'Detached' :      {'IPChi2'     :    9,
                                    'DLS'        :     7},

                 'DetachedHeavy' : {'MinMass'    :  2950 * MeV,
                                    'Pt'         :     0 * MeV,
                                    'MuPt'       :   300 * MeV,
                                    'VertexChi2' :    25,
                                    'IPChi2'     :     0,
                                    'DLS'        :     5},

                 'DetachedJPsi' :  {'MassWindow'  :     120},

                 'DetachedPsi2S' : {'MassWindow'  :     120},

                 # Turbo lines
                 'JPsiTurbo' :          {'MassWindow' :   120 * MeV,
                                         'Pt'         :     0 * MeV},
                 'Psi2STurbo' :         {'MassWindow' :   120 * MeV,
                                         'Pt'         :     0 * MeV},
                 'Psi2SLowPTTurbo' :    {'MassWindow' :   120 * MeV,
                                         'PtMax'      :  2500 * MeV},
                 'UpsilonTurbo' :       {'MinMass'    :   7900 * MeV,
                                         'VertexChi2' :     25,
                                         'PIDCut'     : "MINTREE('mu-' == ABSID, PROBNNmu) > 0.2" },
                 
                 'ChicXTurbo' :         {'MassWindow_comb'  :  100 * MeV,
                                         'VertexChi2'       :  25}, 
                                          
                 'ChicXExtraSelK' :        {'VertexChi2'  :  25},
                 'ChicXExtraSelP' :        {'VertexChi2'  :  25},
                 'ChicXExtraSelPi' :       {'VertexChi2'  :  25},
                 'ChicXExtraSelMu' :       {'VertexChi2'  :  25},
                 'ChicXExtraSelKsDD' :       {'VertexChi2'  :  25},
                 'ChicXExtraSelKsLL' :       {'VertexChi2'  :  25},
                 'ChicXExtraSelElectron' : {'VertexChi2'  :  25},
                 'ChicXExtraSelGamma' :    {'VertexChi2'  :  25},
                 'ChicXExtraSelLambdaDD' :   {'VertexChi2'  :  25},
                 'ChicXExtraSelLambdaLL' :   {'VertexChi2'  :  25},
                 }

    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
            if nickname:
                return self._stages[nickname]
            else:
                return self._stages

        from Stages import (DiMuonFilter, BaseDiMuonFilter, JpsiFilter, Psi2SFilter, Psi2SLowPtFilter, 
                            DetachedCommonFilter, BFilter, BFilterUB, BFilterPrompt, UpsilonFilter, ZFilter, DetachedDiMuonFilter,
                            DetachedDiMuonHeavyFilter, DetachedJpsiFilter,
                            DetachedPsi2SFilter, SoftDiMuonFilter)
        from Stages import ChicJpsiDiMuon
        self._stages = {'DiMuon'        : [DiMuonFilter('DiMuon')],
                        'JPsi'          : [JpsiFilter('JPsi')],
                        'JPsiHighPT'    : [JpsiFilter('JPsiHighPT')],
                        'Psi2S'         : [Psi2SFilter('Psi2S')],
                        'Psi2SLowPT'    : [Psi2SLowPtFilter('Psi2SLowPT')],
                        'Psi2SHighPT'   : [Psi2SFilter('Psi2SHighPT')],
                        'B'             : [BFilter('B')],
                        'BUB'           : [BFilterUB('BUB')],
                        'BPrompt'       : [BFilterPrompt('BPrompt')],
                        'Z'             : [ZFilter('Z')],
                        'Detached'      : [DetachedDiMuonFilter('Detached')],
                        'Soft'          : [SoftDiMuonFilter('Soft')],
                        'DetachedHeavy' : [DetachedDiMuonHeavyFilter('DetachedHeavy')],
                        'DetachedJPsi'  : [DetachedJpsiFilter('DetachedJPsi')],
                        'DetachedPsi2S' : [DetachedPsi2SFilter('DetachedPsi2S')],
                        # Turbo lines
                        'JPsiTurbo'         : [JpsiFilter('JPsiTurbo')],
                        'Psi2STurbo'        : [Psi2SFilter('Psi2STurbo')],
                        'Psi2SLowPTTurbo'   : [Psi2SLowPtFilter('Psi2SLowPTTurbo')],
                        'UpsilonTurbo'      : [UpsilonFilter('UpsilonTurbo')],
                        'ChicXTurbo'        : [ChicJpsiDiMuon],
                        }

        if nickname:
            return self._stages[nickname]
        else:
            return self._stages
    
    def extraAlgos(self):
        from Stages import ChicJpsiDiMuon, ChicXExtraCombiner
        from Inputs import (BiKalmanFittedKaons, BiKalmanFittedPions, BiKalmanFittedProtons,
                            BiKalmanFittedMuons, KsLLTF, KsDD, BiKalmanFittedPhotons, BiKalmanFittedElectrons,
                            LambdaLLTrackFitted, LambdaDDTrackFitted)
        
        extraAlgos = { 'ChicXTurbo' : { 'ChicXExtraSelK'        : [ChicXExtraCombiner('ChicXExtraSelK', '[B+ -> chi_c1(1P) K+]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedKaons])],
                                        'ChicXExtraSelPi'       : [ChicXExtraCombiner('ChicXExtraSelPi', '[B+ -> chi_c1(1P) pi+]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedPions])],
                                        'ChicXExtraSelP'        : [ChicXExtraCombiner('ChicXExtraSelP', '[B+ -> chi_c1(1P) p+]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedProtons])],
                                        'ChicXExtraSelMu'       : [ChicXExtraCombiner('ChicXExtraSelMu', '[B+ -> chi_c1(1P) mu+]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedMuons])],
                                        'ChicXExtraSelKsDD'       : [ChicXExtraCombiner('ChicXExtraSelKsDD', '[B+ -> chi_c1(1P) KS0]cc',
                                                                                      [ChicJpsiDiMuon, KsDD])],
                                        'ChicXExtraSelKsLL'       : [ChicXExtraCombiner('ChicXExtraSelKsLL', '[B+ -> chi_c1(1P) KS0]cc',
                                                                                      [ChicJpsiDiMuon, KsLLTF])],
                                        'ChicXExtraSelGamma'    : [ChicXExtraCombiner('ChicXExtraSelGamma', '[B+ -> chi_c1(1P) gamma]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedPhotons])],
                                        'ChicXExtraSelElectron' : [ChicXExtraCombiner('ChicXExtraSelElectron', '[B+ -> chi_c1(1P) e-]cc',
                                                                                      [ChicJpsiDiMuon, BiKalmanFittedElectrons])],
                                        'ChicXExtraSelLambdaDD'   : [ChicXExtraCombiner('ChicXExtraSelLambdaDD', '[B+ -> chi_c1(1P) Lambda0]cc',
                                                                                      [ChicJpsiDiMuon, LambdaDDTrackFitted])],
                                        'ChicXExtraSelLambdaLL'   : [ChicXExtraCombiner('ChicXExtraSelLambdaLL', '[B+ -> chi_c1(1P) Lambda0]cc',
                                                                                        [ChicJpsiDiMuon, LambdaLLTrackFitted])],
                                        }
                       }
        
        return extraAlgos
    
    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Configurables import HltANNSvc

        stages = self.stages()
        
        for (nickname, algos, extraAlgs) in self.algorithms(stages):
            linename = 'DiMuon' + nickname if nickname != 'DiMuon' else nickname
            if "Turbo" in nickname : 
                Hlt2Line(linename, prescale = self.prescale,
                         algos = algos, extraAlgos = extraAlgs, postscale = self.postscale, Turbo = True,
                         PersistReco=self.getProps()['PersistReco'].get(nickname, False))
            else:
                Hlt2Line(linename, prescale = self.prescale,
                         algos = algos, postscale = self.postscale)

                
            if nickname is 'JPsi':
                HltANNSvc().Hlt2SelectionID.update( { "Hlt2DiMuonJPsiDecision":  50201 } )

            if nickname is 'JPsiTurbo':
                HltANNSvc().Hlt2SelectionID.update( { 'Hlt2DiMuonJPsiTurboDecision':  50212 } )
                
            if nickname is 'ChicXTurbo':
                HltANNSvc().Hlt2SelectionID.update( { 'Hlt2DiMuonChicXTurboDecision':  50312 } )
                    

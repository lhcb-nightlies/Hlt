##
#  @author V. V. Gligorov vladimir.gligorov@cern.ch
# 
#  Please contact the responsible before editing this file
#
##

# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter

from HltTracking.HltPVs import PV3D
from Inputs import Hlt2Muons,Hlt2Electrons,Hlt2Kaons,Hlt2Pions
from Inputs import Hlt2ProbeMuons, Hlt2ProbeElectrons

class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] : 
            if i not in modules : modules.append(i)
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, "TrackEffElectron" + name, code, [tracks], shared = True, nickname = 'TrackGEC')

class DetachedMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPMu)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Mu)s) & (PT> %(PtMu)s) & \
                 (TRCHI2DOF<%(TrChi2Mu)s)  & in_range(%(EtaMinMu)s, ETA, %(EtaMaxMu)s) & (PROBNNmu > %(ProbNNmu)s)")
        inputs = [ Hlt2Muons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedElectronFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPEle)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Ele)s) & (PT> %(PtEle)s) & \
                 (TRCHI2DOF<%(TrChi2Ele)s)  & in_range(%(EtaMinEle)s, ETA, %(EtaMaxEle)s) & (PROBNNe > %(ProbNNe)s)") 
        inputs = [ Hlt2Electrons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, 
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedKaonFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPKaon)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Kaon)s) & (PT> %(PtKaon)s) & \
                 (TRCHI2DOF<%(TrChi2Kaon)s) & (PROBNNk > %(ProbNNk)s)")
        inputs = [ Hlt2Kaons ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedPionFilter(Hlt2ParticleFilter):
    def __init__(self, name, nickname):
        code = ("(MIPDV(PRIMARY)>%(IPPion)s) & (MIPCHI2DV(PRIMARY)>%(IPChi2Pion)s) & (PT> %(PtPion)s) & \
                 (TRCHI2DOF<%(TrChi2Pion)s) & (PROBNNpi > %(ProbNNpi)s)")
        inputs = [ Hlt2Pions ]
        Hlt2ParticleFilter.__init__(self, name, code, inputs,
                                    nickname = nickname,
                                    dependencies = [PV3D('Hlt2')],
                                    shared = True)

class DetachedEKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedElectronFilter('ForEleTPDetElectron','SharedChild'),DetachedKaonFilter('ForEleTPDetKaon','SharedChild')]
        motherMonitor   = (   "process(monitor(M,  mHist_eK , 'eK_mass'))"
                           ">> process(monitor(PT, ptHist_eK, 'eK_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_eK, 'e_eK_ETA'))"
                                    ">> ~EMPTY"
                                   )
                          }
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ K-]cc","[J/psi(1S) -> e+ K+]cc"], 
                                    inputs, 
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_eK     = Gaudi.Histo1DDef('eK_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_eK    = Gaudi.Histo1DDef('eK_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_eK = Gaudi.Histo1DDef('e_eK_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedEPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'e+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedElectronFilter('ForEleTPDetElectron','SharedChild'),DetachedPionFilter('ForEleTPDetPion','SharedChild')]
        motherMonitor   = (   "process(monitor(M,  mHist_ePi , 'ePi_mass'))"
                           ">> process(monitor(PT, ptHist_ePi, 'ePi_PT'  ))"
                           ">> ~EMPTY"
                          )   
        daughterMonitor = { "e+" : (    "process(monitor(ETA, etaHist_e_ePi, 'e_ePi_ETA'))"
                                    ">> ~EMPTY"
                                   )   
                          } 
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> e+ pi-]cc", "[J/psi(1S) -> e+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = ["mHist_ePi     = Gaudi.Histo1DDef('ePi_mass'   , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_ePi    = Gaudi.Histo1DDef('ePi_PT'     , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_e_ePi = Gaudi.Histo1DDef('e_ePi_ETA'  , 0       , 6       , 120)",
                                                 ],  
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedMuKPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'K+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedMuonFilter('ForEleTPDetMuon','SharedChild'),DetachedKaonFilter('ForEleTPDetKaon','SharedChild')]
        motherMonitor   = (   "process(monitor(M,  mHist_muK , 'muK_mass'))"
                           ">> process(monitor(PT, ptHist_muK, 'muK_PT'  ))"
                           ">> ~EMPTY"
                          )   
        daughterMonitor = { "mu+" : (    "process(monitor(ETA, etaHist_mu_muK, 'mu_muK_ETA'))"
                                     ">> ~EMPTY"
                                    )   
                          }  
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ K-]cc","[J/psi(1S) -> mu+ K+]cc"], 
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = ["mHist_muK      = Gaudi.Histo1DDef('muK_mass'    , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muK     = Gaudi.Histo1DDef('muK_PT'      , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_mu_muK = Gaudi.Histo1DDef('mu_muK_ETA'  , 0       , 6       , 120)",
                                                 ],  
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             ) 

class DetachedMuPiPair(Hlt2Combiner) :
    def __init__(self,name):
        dc = {'pi+' : "ALL", 'mu+' : "ALL"}
        cc = ("(AM < %(AM)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2)s) & (BPVDIRA > %(DIRA)s) & (BPVVDCHI2 > %(VDCHI2)s)")
        inputs = [DetachedMuonFilter('ForEleTPDetMuon','SharedChild'),DetachedPionFilter('ForEleTPDetPion','SharedChild')]
        motherMonitor   = (   "process(monitor(M,  mHist_muPi , 'muPi_mass'))"
                           ">> process(monitor(PT, ptHist_muPi, 'muPi_PT'  ))"
                           ">> ~EMPTY"
                          )
        daughterMonitor = { "mu+" : (    "process(monitor(ETA, etaHist_mu_muPi, 'mu_muPi_ETA'))"
                                     ">> ~EMPTY"
                                    )
                          }
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, 
                                    ["[J/psi(1S) -> mu+ pi-]cc", "[J/psi(1S) -> mu+ pi+]cc"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc,
                                    Preambulo  = ["mHist_muPi      = Gaudi.Histo1DDef('muPi_mass'    , 0 * GeV , 6  * GeV, 200)",
                                                  "ptHist_muPi     = Gaudi.Histo1DDef('muPi_PT'      , 0 * GeV , 50 * GeV, 200)",
                                                  "etaHist_mu_muPi = Gaudi.Histo1DDef('mu_muPi_ETA'  , 0       , 6       , 120)",
                                                 ],
                                    MotherMonitor = motherMonitor,
                                    DaughtersMonitors = daughterMonitor
                             )

class DetachedEEKPair(Hlt2Combiner) :
    def __init__(self,name) :
        dc = {"e+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = ("(AM < %(AMTAP)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2TAP)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s)")
        inputs = [DetachedEKPair('DetachedEK'),Hlt2ProbeElectrons]
        preambulo =  [ # With thanks to L. Dufour!
            'from numpy import inner', #if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #
            'TagElectronMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagElectronEnergy = CHILD(CHILD(E,1), 1)',
            #
            'ProbeElectron    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeElectron[:]',
            'ProbeElectron = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #       
            'TagPCosineTheta = inner(ProbeElectron, TagElectronMomentumVector)', # |p_tag| Cos(Theta) 
            #
            'Electron_M = 0.511', # in MeV
            #
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Electron_M**2 - Electron_M**2)/(TagElectronEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeElectron[i] + TagElectronMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagElectronEnergy + math.sqrt(Electron_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)' 
        ]
        #
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,
                                    ["B+ -> J/psi(1S) e+","B- -> J/psi(1S) e-"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc,
                                    MotherCut = mc,
                                    Preambulo  = preambulo
                             )

class DetachedMuMuKPair(Hlt2Combiner) :
    def __init__(self,name) :
        dc = {"mu+" : "ALL", "J/psi(1S)" : "ALL"}
        cc = ("(AM < %(AMTAP)s)")
        mc = ("(VFASPF(VCHI2) < %(VCHI2TAP)s) & in_range(%(MLOW)s, BMassFromConstraint, %(MHIGH)s)")
        inputs = [DetachedMuKPair('DetachedMuK'),Hlt2ProbeMuons]
        preambulo =  [ # With thanks to L. Dufour!
            'from numpy import inner', #if you want you can also calculate all inner products yourself.
            'TagKaonMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2),1), CHILD(CHILD(PZ,2),1)]',
            'TagKaonEnergy            = CHILD(CHILD(E,2),1)',
            #   
            'TagMuonMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
            'TagMuonEnergy = CHILD(CHILD(E,1), 1)',
            #   
            'ProbeMuon    = [CHILD(PX,2),CHILD(PY,2),CHILD(PZ,2)]',
            'ProbeUnnormalised = ProbeMuon[:]',
            'ProbeMuon = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
            #       
            'TagPCosineTheta = inner(ProbeMuon, TagMuonMomentumVector)', # |p_tag| Cos(Theta) 
            #   
            'Muon_M = 105.658', # in MeV
            #   
            # ideally would replace the 3096.9 with a functor to get the PDG mass for the J/Psi(1S) in MeV
            # (there must be a functor for this PDG mass...)
            'Probe_Momentum_From_Mass_constraint = 0.5*(3096.9**2 - Muon_M**2 - Muon_M**2)/(TagMuonEnergy - TagPCosineTheta)',
            'JPsi_momentum = [ Probe_Momentum_From_Mass_constraint*ProbeMuon[i] + TagMuonMomentumVector[i] for i in range (0,3)]',
            'JPsi_energy = TagMuonEnergy + math.sqrt(Muon_M**2 + Probe_Momentum_From_Mass_constraint**2)',
            'BMassFromConstraint = math.sqrt( (JPsi_energy+TagKaonEnergy)**2 - (JPsi_momentum[0]+TagKaonMomentumVector[0])**2 -(JPsi_momentum[1]+TagKaonMomentumVector[1])**2 - (JPsi_momentum[2]+TagKaonMomentumVector[2])**2)' 
        ]   
        #   
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,
                                    ["B+ -> J/psi(1S) mu+","B- -> J/psi(1S) mu-"],
                                    inputs,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    tistos = 'TisTosSpec',
                                    DaughtersCuts = dc, CombinationCut = cc, 
                                    MotherCut = mc, 
                                    Preambulo  = preambulo
                             ) 

#!/usr/bin/env python
# =============================================================================
# @file   Stages.py
# @author Carla Marin Benito (carla.marin.benito@cern.ch)
# @author Regis Lefevre (rlefevre@cern.ch)
# @author Max Chefdeville (chefdevi@lapp.in2p3.fr)
# @date   24.02.2017
# =============================================================================
"""Candidate building stages for HLT2 Calo PID lines. Based on:
   - Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBeauty2XGammaExclusive.py"""

### Build PVs
from HltTracking.HltPVs import PV3D
from GaudiKernel.SystemOfUnits import MeV

### Hlt2ParticleFilter (equivalent to FilterDesktop)
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

# B2XGamma: Photon filter
class PhotonFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        cut = ("(PT > %(Photon_PT)s)")
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    shared=True,
                                    nickname=nickname)

# B2XGamma: HH filter
class HHFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        pream = [("goodTrack = ((MIPCHI2DV(PRIMARY) > %(Track_IP_Chi2)s)"
                               " & (TRCHI2DOF < %(Track_Chi2ndof)s )"
                               " & (P  > %(Track_P)s )"
                               " & (PT > %(Track_PT)s ))"),
                 
                 ("goodHH = ((VFASPF(VCHI2/VDOF) < %(HH_Vtx_Chi2ndof)s)"
                            " & (ADMASS('%(HH_Particle)s') < %(HH_Mass_Range)s ))")]
        cut = ("(CHILDCUT( goodTrack, 1 ))"
               " & (CHILDCUT( goodTrack, 2 ))"
               " & goodHH")
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    Preambulo=pream,
                                    dependencies = [PV3D('Hlt2')],
                                    nickname=nickname)
        
# D02KPiPi0: Pi0 filter
class Pi0Filter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, resolved=False, nickname = None):
        cut = ("(PT > %(Pi0_PT_MIN)s *MeV) & (ID == 'pi0')")
        if resolved:
            cut += "& (CHILD(CL,1) > %(ResolvedPi0_GammaCL_MIN)s)"
            cut += "& (CHILD(CL,2) > %(ResolvedPi0_GammaCL_MIN)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    shared=True,
                                    nickname=nickname)

# D02KPiPi0: Track filter
class TrackFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, track, nickname = None):
        cut = ("(TRCHI2DOF < %(Track_CHI2DOF_MAX)s)" +
               "& (TRGHOSTPROB < %(Track_GHOSTPROB_MAX)s)")
        if track=="SoftPion":
            cut += "& (MIPCHI2DV(PRIMARY) < %(SoftPion_MIPCHI2DV_MAX)s)"
        else:
            cut += "& (MIPCHI2DV(PRIMARY) > %(Track_MIPCHI2DV_MIN)s)"
        if track=="Pion":
            cut += "& (PIDK < %(Pion_PIDK_MAX)s)"
        elif track=="Kaon":
            cut += "& (PIDK > %(Kaon_PIDK_MIN)s)"
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    dependencies = [PV3D('Hlt2')],
                                    shared=True,
                                    nickname=nickname)      

### Hlt2Combiner (supports CombineParticles)
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner

# B2XGamma combiner
class B2XGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        comb_cuts = ("(ADAMASS('%(B_Particle)s') < 1.5*%(B_Mass_Range)s)")
        moth_cuts = ("(acos(BPVDIRA) < %(B_DIRA)s)"
                     " & (BPVIPCHI2() < %(B_IP_Chi2)s)"
                     " & (PT > %(B_PT)s)"
                     " & (ADMASS('%(B_Particle)s') < %(B_Mass_Range)s)"
                     " & (BPVVDCHI2 > %(B_FD_Chi2)s)")

        # Monitoring
        pream_moni = []
        mother_moni = ''
        # fix me: can this be obtained from %(B_Particle)s and %(HH_Particle)s?
        if nickname == "Bd2KstGamma":
            pream_moni = ['B_Mass  = 5279.63 * MeV',
                          'HH_Mass = 891.76 * MeV']
        elif nickname == "Bs2PhiGamma":
            pream_moni = ['B_Mass  = 5366.89 * MeV',
                          'HH_Mass = 1019.46 * MeV']
        if nickname in ["Bd2KstGamma", "Bs2PhiGamma"]:
            pream_moni += [
                "B_Mass_Min  = (B_Mass - 1.2 * %(B_Mass_Range)s)",
                "B_Mass_Max  = (B_Mass + 1.2 * %(B_Mass_Range)s)",
                "HH_Mass_Min  =(HH_Mass - 1.2 * %(HH_Mass_Range)s)",
                "HH_Mass_Max  =(HH_Mass + 1.2 * %(HH_Mass_Range)s)",
                "BMassHisto  = Gaudi.Histo1DDef('b_mass', B_Mass_Min, B_Mass_Max, 200)",
                "HHMassHisto = Gaudi.Histo1DDef('hh_mass', HH_Mass_Min, HH_Mass_Max, 200)",
                ]
            mother_moni = (
                "process(monitor(M, BMassHisto, 'b_mass'))"
                ">> process(monitor(CHILD(M, ABSID == '%(HH_Particle)s'), HHMassHisto, 'hh_mass'))"
                ">> ~EMPTY")

        Hlt2Combiner.__init__(self, name, decay, inputs,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              Preambulo = pream_moni,
                              MotherMonitor = mother_moni)


# Ds2KKpi: Ds filter (taken from StrippingD2hhhFTCalib_KKPLine and removing cuts already applied in Hlt2Shared_Ds)
class DsFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        cut = ("(CHILD(PIDK-PIDpi,3) < 3.0)"
               "& (CHILD(PIDK-PIDpi,1) > 7.0)"
               "& (CHILD(PIDK-PIDpi,2) > 7.0)"
               "& ((CHILD(PT,1) + CHILD(PT,2) + CHILD(PT,3)) > 3200.0*MeV)"
               "& (2 <= NINGENERATION((MIPCHI2DV(PRIMARY) > 10.0 ) , 1))"
               "& (PT > 2000.0)"
               "& (VFASPF(VCHI2/VDOF) < 6.0)"
               "& (BPVDIRA > 0.99995)"
               "& (BPVIPCHI2() < 1500.0)"
               "& (VFASPF(VMINVDCHI2DV(PRIMARY)) > 150.0)"
               "& (BPVLTIME() > 0.2*picosecond)"
               "& (DOCACHI2MAX < 50.0)")
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    shared=True,
                                    nickname=nickname)

# Dsst2DsGamma combiner
class Dsst2DsGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        daug_cuts = {'gamma' : "(PT > %(gamma_PT)s)"}
        comb_cuts = ("ATRUE")
        moth_cuts = ("(BPVDIRA > %(Dsst_DIRA)s)"
                     " & (BPVIP() < %(Dsst_IP)s)"
                     " & (BPVIPCHI2() < %(Dsst_IP_Chi2)s)"
                     " & (MM > %(Dsst_Mass_Min)s)"
                     " & (MM < %(Dsst_Mass_Max)s)")

        # Monitoring
        pream_moni = [
            "Dsst_Mass_Range = %(Dsst_Mass_Max)s - %(Dsst_Mass_Min)s",
            "Dsst_MH_Min = %(Dsst_Mass_Min)s - 0.2 * Dsst_Mass_Range",
            "Dsst_MH_Max = %(Dsst_Mass_Max)s + 0.2 * Dsst_Mass_Range",
            "DsstMassHisto = Gaudi.Histo1DDef('dsst_mass', Dsst_MH_Min, Dsst_MH_Max, 400)",
            "DsMassHisto   = Gaudi.Histo1DDef('ds_mass', 1926, 2010, 400)", # limits from Hlt2SharedParticles.Ds
            ]
        mother_moni = (
            "process(monitor(M, DsstMassHisto, 'dsst_mass'))"
            ">> process(monitor(CHILD(M, ABSID == 'D_s+'), DsMassHisto, 'ds_mass'))"
            ">> ~EMPTY")

        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              Preambulo = pream_moni,
                              MotherMonitor = mother_moni)

# D02KPiPi0 combiner
class D02KPiPi0Combiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        track_cuts  = ("(PT > %(Track_PT_MIN)s *MeV)")
        daught_cuts = { 'K-' : track_cuts, 'pi+' : track_cuts, 'pi0' : "ALL" }
        comb_cuts = ("(in_range( %(D0_AM_MIN)s *MeV, AM, %(D0_AM_MAX)s *MeV))")
        moth_cuts = ("(PT > %(D0_PT_MIN)s)"+
                     "& (VFASPF(VCHI2PDOF) < %(D0_VCHI2PDOF_MAX)s)"+
                     "& (BPVVDCHI2 > %(D0_VVDCHI2_MIN)s)" +
                     "& (BPVIPCHI2() < %(D0_VIPCHI2_MAX)s)" +
                     "& (BPVDIRA > %(D0_DIRA_MIN)s)" +
                     "& (in_range( %(D0_M_MIN)s *MeV, M, %(D0_M_MAX)s *MeV))")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daught_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# Etap2PiPiGamma combiner
class Etap2PiPiGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        pi_daug_cuts = ("(PT > %(Pi_PT)s)"
                        "& (MIPCHI2DV(PRIMARY) > %(Pi_MIPCHI2DV)s) "
                        "& (TRCHI2DOF < %(Pi_Track_Chi2ndof)s) "
                        "& (TRGHOSTPROB < %(Pi_TRGHOSTPROB)s) "
                        "& (P > %(Pi_P)s) "
                        "& (PIDK-PIDpi < %(Pi_PID)s)")
        g_daug_cuts = ("(PT > %(gamma_PT)s)")
        daug_cuts = {'pi+' : pi_daug_cuts, 'gamma' : g_daug_cuts}
        comb_cuts = ("ATRUE")
        moth_cuts = ("(MM > %(Etap_Mass_Min)s) "
                     "& (MM < %(Etap_Mass_Max)s)")
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname)

# D2EtapPi combiner
class D2EtapPiCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        etap_daug_cuts = ("ALL")
        pi_daug_cuts = ("(PT > %(Pi_BACH_PT)s) & (MIPCHI2DV(PRIMARY) > %(Pi_MIPCHI2DV)s) & (TRCHI2DOF < %(Pi_Track_Chi2ndof)s) & (TRGHOSTPROB < %(Pi_TRGHOSTPROB)s) & (P > %(Pi_P)s) ")
        daug_cuts = {'eta_prime' : etap_daug_cuts , 'pi+' : pi_daug_cuts}
        comb_cuts = ("(APT > %(D_APT)s) & (in_range( %(D_Mass_Min)s,AM,%(D_Mass_Max)s))")
        moth_cuts = ("(VFASPF(VCHI2PDOF) < %(D_Vtx_Chi2ndof)s) & (BPVLTIME() > %(D_BPVLTIME)s) & (BPVDIRA>%(D_DIRA)s) & (BPVIP()<%(D_IP)s) & (BPVIPCHI2()<%(D_IP_Chi2)s)")
        #moth_cuts = ("(VFASPF(VCHI2PDOF) < %(D_Vtx_Chi2ndof)s) & (BPVLTIME() > %(D_BPVLTIME)s)")

        # Monitoring
        pream_moni = [
            "D_Mass_Range = %(D_Mass_Max)s - %(D_Mass_Min)s",
            "D_MH_Min = %(D_Mass_Min)s - 0.2 * D_Mass_Range",
            "D_MH_Max = %(D_Mass_Max)s + 0.2 * D_Mass_Range",
            "Etap_Mass_Range = %(Etap_Mass_Max)s - %(Etap_Mass_Min)s",
            "Etap_MH_Min = %(Etap_Mass_Min)s - 0.2 * Etap_Mass_Range",
            "Etap_MH_Max = %(Etap_Mass_Max)s + 0.2 * Etap_Mass_Range",
            "DMassHisto    = Gaudi.Histo1DDef('d_mass', D_MH_Min, D_MH_Max, 400)",
            "EtapMassHisto = Gaudi.Histo1DDef('etap_mass', Etap_MH_Min, Etap_MH_Max, 400)",
            ]
        mother_moni = (
            "process(monitor(M, DMassHisto, 'd_mass'))"
            ">> process(monitor(CHILD(M, ABSID == 'eta_prime'), EtapMassHisto, 'etap_mass'))"
            ">> ~EMPTY")

        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              Preambulo = pream_moni,
                              MotherMonitor = mother_moni)

# DstD02KPiPi0 combiner        
class DstD02KPiPi0Combiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        comb_cuts = ("(in_range( %(Dst_dAM_MIN)s *MeV, (AM - AM1), %(Dst_dAM_MAX)s *MeV))")
        moth_cuts = ("(PT > %(Dst_PT_MIN)s)"+
                     "& (VFASPF(VCHI2PDOF) < %(Dst_VCHI2PDOF_MAX)s)"+
                     "& (BPVVDCHI2 < %(Dst_VVDCHI2_MAX)s)" +
                     "& (BPVIPCHI2() < %(Dst_VIPCHI2_MAX)s)" +
                     "& (in_range( %(Dst_dM_MIN)s *MeV, (M - M1), %(Dst_dM_MAX)s *MeV))")

        # Monitoring
        pream_moni = [
            "D0_Mass_Range = %(D0_M_MAX)s - %(D0_M_MIN)s",
            "D0_MH_Min = %(D0_M_MIN)s - 0.2 * D0_Mass_Range",
            "D0_MH_Max = %(D0_M_MAX)s + 0.2 * D0_Mass_Range",
            "Dst_dMass_Range = %(Dst_dM_MAX)s - %(Dst_dM_MIN)s",
            "Dst_dMH_Min = %(Dst_dM_MIN)s - 0.2 * Dst_dMass_Range",
            "Dst_dMH_Max = %(Dst_dM_MAX)s + 0.2 * Dst_dMass_Range",
            "DstMassHisto   = Gaudi.Histo1DDef('dst_mass'  , 1700 * MeV, 2300 * MeV, 400)",
            "D0MassHisto    = Gaudi.Histo1DDef('d0_mass'   , D0_MH_Min, D0_MH_Max, 400)",
            "Pi0MassHisto   = Gaudi.Histo1DDef('pi0_mass'  , 65 * MeV, 205 * MeV)", # from Hlt2SharedParticles.Pi0
            "DeltaMassHisto = Gaudi.Histo1DDef('delta_mass', Dst_dMH_Min, Dst_dMH_Max, 400)",
            ]
        mother_moni = (
            "process(monitor(M, DstMassHisto, 'dst_mass'))"
            ">> process(monitor(CHILD(M, ABSID == 'D0'), D0MassHisto, 'd0_mass'))"
            ">> process(monitor(CHILD(M, ABSID == 'pi0'), Pi0MassHisto, 'pi0_mass'))"
            ">> process(monitor(M - CHILD(M, ABSID == 'D0'), DeltaMassHisto, 'delta_mass'))"
            ">> ~EMPTY")

        Hlt2Combiner.__init__(self, name, decay, inputs,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              Preambulo = pream_moni,
                              MotherMonitor = mother_moni)

# MuMu Combiner
class DiMuonNoIPCombiner(Hlt2Combiner) :
    def __init__(self, name , decay, inputs , nickname = None):
        decay = decay
        dc = { 'mu+' : "(PT > %(MuPT)s) "
               "& (P > %(MuP)s) "
               "& (TRGHOSTPROB < %(GhostProb)s) "
               "& (PROBNNmu > %(MuProbNNmu)s) "}
        cc = ("(AMAXDOCA('') < %(DOCA)s)")
        mc = ("(VFASPF(VCHI2PDOF) < %(VChi2)s) ")

        Hlt2Combiner.__init__(self, name, decay , inputs,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              DaughtersCuts  = dc,
                              CombinationCut = cc,
                              MotherCut      = mc)

# Promt MuMu Filter
class PrmptDiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None):
        cut = ("(MINTREE('mu+'==ABSID,PT) > %(MuPT)s) & (MINTREE('mu+'==ABSID,P) > %(MuP)s)"
               "& (MINTREE('mu+'==ABSID,BPVIPCHI2()) < %(MuIPChi2)s)"
               "& (MINTREE('mu+'==ABSID,PROBNNmu) > %(MuProbNNmu)s)"
               "& (CHILD(PT,1)*CHILD(PT,2) > %(MuPTPROD)s)"
               "& (PT > %(DiMu_PT)s)"
               "& (HASVERTEX)"
               "& (BPVVDCHI2 < %(DiMu_FDChi2)s)")
        
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    tistos = 'TisTosSpec',
                                    dependencies = [PV3D('Hlt2')],
                                    nickname=nickname)

# Eta2MuMuGamma combiner
class Eta2MuMuGammaCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None):
        decay = decay
        daug_cuts = {'gamma' : '(PT > %(gamma_PT)s)'}
        comb_cuts = ("(in_range( %(Eta_Mass_Min)s , AM , %(Eta_Mass_Max)s))")
        moth_cuts = ("(PT > %(Eta_PT)s)")

        # Monitoring
        pream_moni = [
            "Eta_Mass_Range = %(Eta_Mass_Max)s - %(Eta_Mass_Min)s",
            "Eta_MH_Min = %(Eta_Mass_Min)s - 0.2 * Eta_Mass_Range",
            "Eta_MH_Max = %(Eta_Mass_Max)s + 0.2 * Eta_Mass_Range",
            "EtaMassHisto    = Gaudi.Histo1DDef('eta_mass', Eta_MH_Min, Eta_MH_Max, 400)"
            ]
        mother_moni = (
            "process(monitor(M, EtaMassHisto, 'eta_mass'))"
            ">> ~EMPTY")
        
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              DaughtersCuts = daug_cuts,
                              CombinationCut=comb_cuts,
                              MotherCut=moth_cuts,
                              dependencies = [PV3D('Hlt2')],
                              nickname=nickname,
                              Preambulo = pream_moni,
                              MotherMonitor = mother_moni)



##########
### Build intermediate states (final ones are built in Lines.py)
##########
        
# B2XGamma
from Inputs import Hlt2Photons, Hlt2NoPIDsPions, Hlt2Kstar, Hlt2Phi, Hlt2Ds, Hlt2Muons
photons_B2XGamma = PhotonFilter("photons_B2XGamma",
                                [Hlt2Photons],
                                nickname="Bd2KstGamma")

Kstar_Bd2KstGamma = HHFilter("Kstar_Bd2KstGamma",
                             [Hlt2Kstar],
                             nickname="Bd2KstGamma")

Phi_Bs2PhiGamma = HHFilter("Phi_Bs2PhiGamma",
                           [Hlt2Phi],
                           nickname="Bs2PhiGamma")

# D2EtapPi
Etap_D2EtapPi = Etap2PiPiGammaCombiner("Etap_D2EtapPi",
                                       "[eta_prime -> pi+ pi- gamma]cc",
                                       [Hlt2NoPIDsPions,Hlt2Photons],
                                       nickname="D2EtapPi")

# Dsst2DsGamma
Ds_Dsst2DsGamma = DsFilter("Ds_Dsst2DsGamma",
                           [Hlt2Ds],
                           nickname="Dsst2DsGamma")


# D02KPiPi0
from Inputs import Hlt2MergedPi0, Hlt2ResolvedPi0, Hlt2LooseKaons, Hlt2LoosePions, Hlt2NoPIDsPions
MergedPi0_D02KPiPi0 = Pi0Filter("MergedPi0_D02KPiPi0",
                                [Hlt2MergedPi0],
                                resolved=False,
                                nickname="D02KPiPi0_MergedPi0")

ResolvedPi0_D02KPiPi0 = Pi0Filter("ResolvedPi0_D02KPiPi0",
                                  [Hlt2ResolvedPi0],
                                  resolved=True,
                                  nickname="D02KPiPi0_ResolvedPi0")

Kaon_D02KPiPi0 = TrackFilter("Kaon_D02KPiPi0",
                             [Hlt2LooseKaons],
                             track="Kaon",
                             nickname="Kaon_D02KPiPi0")

Pion_D02KPiPi0 = TrackFilter("Pion_D02KPiPi0",
                             [Hlt2LoosePions],
                             track="Pion",
                             nickname="Pion_D02KPiPi0")

SoftPion_DstD02KPiPi0 = TrackFilter("SoftPion_DstD02KPiPi0",
                                    [Hlt2NoPIDsPions],
                                    track="SoftPion",
                                    nickname="SoftPion_DstD02KPiPi0")

# Eta2MuMuGamma
DiMu_Eta2MuMuGamma = DiMuonNoIPCombiner('DiMu_Eta2MuMuGamma',
                                        '[KS0 -> mu+ mu-]cc',
                                        [Hlt2Muons],
                                        nickname="Eta2MuMuGamma")

PrmptDiMu_Eta2MuMuGamma = PrmptDiMuonFilter('PrmptDiMu_Eta2MuMuGamma',
                                            [DiMu_Eta2MuMuGamma],
                                            nickname='Eta2MuMuGamma')

# EOF

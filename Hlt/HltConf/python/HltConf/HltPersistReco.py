"""Configures the persistence of reconstructed objects."""

import itertools
from pprint import pformat
from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from Configurables import GaudiSequencer as Sequence
from Configurables import HltPackedDataWriter
from Configurables import HltLinePersistenceSvc

from GaudiConf.PersistRecoConf import PersistRecoPacking
from Configurables import CaloProcessor

__author__ = "Sean Benson, Rosen Matev"


class HltPersistRecoConf(LHCbConfigurableUser):
    # python configurables to be applied before me
    __queried_configurables__ = [
    ]
    # python configurables that I configure
    __used_configurables__ = [
        (CaloProcessor, "SharedCaloProcessor"),
    ]

    __slots__ = {
        "PackingInputs":   {},
        # Map line names to list of locations after cloning
        "ClonedLineLocations": {},
        "Sequence":        None,
        "OutputLevel":     WARNING,
        "DataType":        "2017",
    }

    def __apply_configuration__(self):
        """Apply the HLT persist reco configuration."""

        from Configurables import PackParticlesAndVertices
        persistRecoSeq = self.getProp("Sequence")
        if not self.getProp("Sequence"):
            return

        persistRecoSeq.IgnoreFilterPassed = False
        persistRecoSeq.Members = []

        # Setup packers and add them to the sequence
        packing = PersistRecoPacking(self.getProp("DataType"),inputs=self.getProp("PackingInputs"))
        packerAlgs = packing.packers()
        packerAlgs.append(PackParticlesAndVertices(
            InputStream='/Event/Turbo', # TODO
            EnableCheck=False,
            VetoedContainers=packing.inputs.values(), # TODO
            AlwaysCreateContainers=(
                self.packedPhysLocations() +
                self.packedRecLocations()
            )
        ))
        for alg in packerAlgs:
            alg.AlwaysCreateOutput = True
            alg.DeleteInput = False
            alg.OutputLevel = self.getProp("OutputLevel")
        persistRecoSeq.Members += packerAlgs

        # Configure HltPackedDataWriter algorithm to add to the raw banks
        pdwriter = HltPackedDataWriter("Hlt2PackedDataWriter")
        pdwriter.PackedContainers = packing.packedLocations()
        pdwriter.PackedContainers += self.packedPhysLocations()
        pdwriter.PackedContainers += self.packedRecLocations()
        pdwriter.ContainerMap = packing.inputToPackedLocationMap()
        persistRecoSeq.Members += [pdwriter]

        # Configure the persistence service with the locations that will be
        # persisted, which will be saved in the TCK to be used by Tesla whilst
        # streaming later
        # The PackedDataWriter changes some unpacked locations to corresponding
        # packed ones, so we perform the same substitution here
        clonedLineLocations = self.getProp("ClonedLineLocations")
        persistedLineLocations = {}
        for line, locs in clonedLineLocations.items():
            persistedLocs = [pdwriter.ContainerMap.get(l, l) for l in locs]
            persistedLineLocations[line] = persistedLocs
        HltLinePersistenceSvc().Locations = persistedLineLocations
        persistedObjectLocations = set(sum(persistedLineLocations.values(), []))

        log.info('Will pack these PersistReco locations: %s', pformat(packing.inputs))
        log.info('Will write these packed locations: %s', pformat(pdwriter.PackedContainers))
        log.info('Will map these locations: %s', pformat(pdwriter.ContainerMap))
        log.info(HltLinePersistenceSvc())

        # register all possible prefixed TES locations created by lines
        from HltLine.HltLine import hlt2Lines
        allOutputSelections = filter(None, sum([l.allOutputSelections() for l in hlt2Lines()], []))
        allPrefixedSelections = self.__turboLocations(allOutputSelections)

        # register locations of referenced objects that we don't want to persist
        externalLocations = self.__turboLocations(packing.externalLocations())

        # Register the mapping of output locations and integers
        self.__registerToHltANNSvc(
            allPrefixedSelections +
            packing.packedLocations() +
            externalLocations +
            self.packedPhysLocations() +
            self.packedRecLocations() +
            list(persistedObjectLocations) +
            self.__allConfiguredLocations()
        )

    def packedPhysLocations(self):
        return [
                '/Event/Turbo/pPhys/Particles',
                '/Event/Turbo/pPhys/Vertices',
                '/Event/Turbo/pPhys/RecVertices',
                '/Event/Turbo/pPhys/FlavourTags',
                '/Event/Turbo/pPhys/Relations',
                '/Event/Turbo/pPhys/PartToRelatedInfoRelations',
                '/Event/Turbo/pPhys/P2IntRelations'
            ]

    def packedRecLocations(self):
        return [
            '/Event/Turbo/pRec/ProtoP/Custom',
            '/Event/Turbo/pRec/Muon/CustomPIDs',
            '/Event/Turbo/pRec/Rich/CustomPIDs',
            '/Event/Turbo/pRec/Track/Custom'
        ]

    def __registerToHltANNSvc(self, locations):
        """Register the packed object locations in the HltANNSvc."""
        from Configurables import HltANNSvc
        from collections import OrderedDict
        # remove duplicate locations, keeping only the first occurance
        # and preserving ordering
        locations = OrderedDict.fromkeys(locations)
        location_ids = {loc: i+1 for i, loc in enumerate(locations)}
        HltANNSvc().PackedObjectLocations = location_ids

    def __allConfiguredLocations(self):
        """Return all input/output locations defined on all used configurables.

        Employs some ugly heuristics to gather all location-holding properties
        of *all* configured configurables:
            1. Property name must contain either Input, Output, Location
               or Container;
            2. Property name must be a string or an iterable of strings;
            3. Property value must contain at least one slash `/` and no double
            colons `::` (to avoid match tool names)
        """
        # Match configurable properties that contain at least one of these
        needles = ['Input', 'Output', 'Location', 'Container']

        locations = []
        for configurable in self.allConfigurables.values():
            if isinstance(configurable, ConfigurableUser):
                continue
            for name, value in configurable.getProperties().iteritems():
                if not any(n in name for n in needles) or not value:
                    continue

                try:
                    stringlike = all(map(lambda x: isinstance(x, str), value))
                except TypeError:
                    continue
                listlike = isinstance(value, list)
                # This will only match string locations, not lists of locations
                locationlike = '/' in value and not '::' in value

                if stringlike and (locationlike or listlike):
                    if listlike:
                        value = [v.replace('TES:', '') for v in value]
                        locations.extend(value)
                    else:
                        value = value.replace('TES:', '')
                        locations.append(value)
        locations.sort()
        return locations + self.__turboLocations(locations,
                                                 assert_no_prefix=False)

    def __turboLocations(self, locations, assert_no_prefix=True):
        """Return the Turbo-prefixed locations."""
        turbo_locations = []
        for loc in locations:
            if not loc.startswith(('Turbo/', '/Event/Turbo')):
                loc = loc.replace('/Event/', '')
                turbo_locations.append('/Event/Turbo/' + loc)
            elif assert_no_prefix:
                raise ValueError('The location {} is already prefixed'
                                 .format(loc))
        return turbo_locations

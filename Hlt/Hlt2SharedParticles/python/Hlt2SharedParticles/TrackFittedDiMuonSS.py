### @file
#
#  Standard Track Fitted same sign Di-muon
#
#  @author Yanxi ZHANG [yanxi.zhang@cern.ch]
#  @date 2016-10-30
#
##
from Gaudi.Configuration import *
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedMuons
from Configurables import CombineParticles, FilterDesktop
from HltLine.HltLine import bindMembers, Hlt2Member

__all__ = ( 'TrackFittedDiMuonSS' )


Hlt2SharedTrackFittedDiMuonSS = Hlt2Member( CombineParticles
                                          , "TrackFittedDiMuonSS"
                                          , DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"
                                          , CombinationCut = "AALL"
                                          , MotherCut = "(VFASPF(VCHI2PDOF)<25)"
                                          , Inputs = [ BiKalmanFittedMuons ]
                                          , WriteP2PVRelations = False
                                          )

TrackFittedDiMuonSS = bindMembers( "Shared", [ BiKalmanFittedMuons, Hlt2SharedTrackFittedDiMuonSS ] )

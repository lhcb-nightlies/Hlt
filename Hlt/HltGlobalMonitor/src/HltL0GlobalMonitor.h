#ifndef HLTL0GLOBALMONITOR_H
#define HLTL0GLOBALMONITOR_H 1

#include <unordered_map>

#include "Event/L0DUReport.h"
#include "Event/ODIN.h"

#include "GaudiKernel/StringKey.h"
#include "HltBase/HltBaseAlg.h"

/** @class HltL0GlobalMonitor HltL0GlobalMonitor.h
 *
 *  functionality:
 *        monitor the L0 decisions seen by Hlt
 *
 *
 *  @author Johannes Albrecht
 *  @date   2010-03-24
 *
 *  original version from Arthur Maciel (2003)
 *
 */

class HltL0GlobalMonitor : public HltBaseAlg {
public:
  /// Standard constructor
  HltL0GlobalMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution


private:

  void monitorL0DU(const LHCb::L0DUReport*);

  template <typename T> T* fetch(const std::string& location, bool warn = true ) {
     if (location.empty()) return nullptr;
     T* t =  this->getIfExists<T>( location ) ;
     if (!t && warn && this->msgLevel(MSG::WARNING) ) Warning( " could not retrieve "  + location,StatusCode::SUCCESS,10).ignore();
     return t;
  }

  Gaudi::Property<std::string> m_stage{this, "Stage", "Hlt1"};
  Gaudi::Property<std::string> m_ODINLocation{this, "ODIN", LHCb::ODINLocation::Default};
  Gaudi::Property<std::string> m_decReportsLocation{this, "DecReports", LHCb::HltDecReportsLocation::Default};
  Gaudi::Property<std::string> m_L0DUReportLocation{this, "L0DUReport", LHCb::L0DUReportLocation::Default};
  Gaudi::Property<std::string> m_hltDecision{this, "HltDecName", "Hlt1Global"};

  AIDA::IHistogram1D* m_L0Input;
  AIDA::IHistogram1D* m_histL0Enabled;
  AIDA::IHistogram1D* m_histL0Disabled;
  AIDA::IHistogram1D* m_histL0EnabledHLT;

  using CounterMap = std::unordered_map<unsigned int, StatEntity*>;
  CounterMap m_l0Counters;

  unsigned int m_nboflabels = 0;
  unsigned int m_lastL0TCK = 0;

};
#endif // HLTL0GLOBALMONITOR_H
